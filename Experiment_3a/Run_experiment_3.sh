#! /bin/bash

echo "Running..."

for i in Building_m*/; do
    cd $i; \
    echo $i; \
    ./run.sh; \
    cd ..; \
done

echo "done"
