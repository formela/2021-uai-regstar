
# Effect of increasing number of memeory elements


This is a directory where our third experiment is performed on a building with one floor.


results/
   - a directory
   - it contains our automatically generated results:
       Experiment3a_report.pdf
          - a detailed summary of the obtained results
          - for each number of memory elements, we analyze
            the obtained strategy values, number of iterations,
            and the execution times (in seconds, user time + system time)
       Table2a.txt
          - source data for Table 2

Template_building_r10_c4.blin
   - Plan of a one-floor building with 4 nodes representing a corridor
     and 10 nodes representing rooms (possible targets with blindness 0.1,
     target value 100, and attack time 100)
   - the structure is:
                    R       R       R       R
                    |       |       |       |
                    5       5       5       5
                    |       |       |       |
            R - 5 - C - 2 - C - 2 - C - 2 - C - 5 - R
                    |       |       |       |
                    5       5       5       5
                    |       |       |       |
                    R       R       R       R
   - each edge with length 2 stands for walking,
     the edges with length 5 represent unlocking doors
     and entering the particular rooms.


Prepare_experiment_3.sh
   - a bash script
   - generates input graphs with 1..8 memory elements to subdirectories
     Building_m_00[1-8]/

Run_experiment_3.sh
   - a bash script
   - runs the strategy computation (takes a bit longer time)

Summarize_experiment_3.sh
   - a bash script
   - extracts data from files Building_m_00[1-8]/*.out
     Building_m_00[1-8]/*.time, and creates the PDF file
     Experiment3a_report.pdf and Table2a.txt stored in ./results/


All the bash scripts uses Makefile and other programs from ../tools/.
