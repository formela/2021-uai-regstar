#! /bin/bash

echo "Summarization of the results..."

for d in Building_m_*/; do
    i=${d%%/}
    cd $i; \
    echo $i; \
    make $i.bl.out; \
    make blreport; \
    make $i.fbtimes; \
    cd ..; \
done

pdfjam --fitpaper true --rotateoversize true -o Experiment3a_report.pdf */*blreport.pdf
echo "Joint report in Experiment3a_report.pdf"

echo "done"

echo >Table2a.txt;

for d in Building_m_*/; do
    i=${d%%/}
    echo -n $i | sed "s/Building_m_0*//" >>Table2a.txt;
    echo -n "   & " >>Table2a.txt;
    tail -n +2 $i/$i.bl.vals | Rscript -e 'data <- as.numeric (readLines ("stdin")); m<-max(data); num<-sum(data > -100); num_close <- sum(data >= 0.9*m); m; num_close/num *100 ;' | sed "s/^....//" | tr '\n' 'm' | sed "s/m/ \& /" |sed "s/m/\\\% \& \$/" >>Table2a.txt;
    cat $i/$i.bl.time | bc | Rscript -e 'data <- as.numeric (readLines ("stdin")); mean(data); sd(data);' | sed "s/^....//" | tr '\n' 'x' | sed "s/x/ \\\\pm /" |sed "s/x/\$   \\\\\\\ /" >>Table2a.txt;
    echo >>Table2a.txt;
done

