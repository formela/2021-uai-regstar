
# Application to a Real-World Patrolling Graph - Experiment 2


This is a directory where our second experiment is performed.


results/
   - a directory
   - it contains our automatically generated results:
       Experiment2_report.pdf
          - a detailed summary of the obtained results
          - for each number of memory elements, we analyze
            the obtained strategy values, number of iterations,
            and the execution times (in seconds, user time + system time)


map_Montreal.kml
   - a dataset generated from https://www.google.com/maps/d/u/0/ 
     by the following steps:
        - create a new map
        - search for Montreal
        - search for the required targets (ATM, bank)
        - add the points to the current map (in the context menu,
          choose "+ Add to map")
        - in the map menu (three dots next to "Untitled map"), choose
           "Export to KML/KMZ",
           change "Entire map" to "Untitled layer",
           check "Export as KML instead of KMZ. Does not support all icons.",
           click "Download".

map_Montreal.png
map_Montreal_crop.png
   - screenshot of the map with the chosen points


Prepare_experiment_2.sh
   - a bash script
   - generates input graphs with 1..4 memory elements to subdirectories
     Mont_m_00[1-4]/

Run_experiment_2.sh
   - a bash script
   - runs the strategy computation (takes a bit longer time)

Summarize_experiment_2.sh
   - a bash script
   - extracts data from files Mont_m_00[1-4]/*.out
     Mont_m_00[1-4]/*.time, and creates the PDF file
     Experiment2_report.pdf stored in ./results/


All the bash scripts uses Makefile and other programs from ../tools/.
