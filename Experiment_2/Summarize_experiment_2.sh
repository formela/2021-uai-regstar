#! /bin/bash

echo "Summarization of results..."

for i in Mont_m_*; do
    cd $i; \
    echo $i; \
    make map_$i.bl.out; \
    make blreport; \
    cd ..; \
done

pdfjam --fitpaper true --rotateoversize true -o Experiment2_report.pdf */*blreport.pdf
echo "Joint report in Experiment2_report.pdf"

echo "done"
