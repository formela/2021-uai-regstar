#! /bin/bash

# number of experiments to be run
EXP_NUM=4

# number of executions in each experiment
NUM_OF_EXECUTIONS=100

# number of trials in each execution
TRIALS_PER_EXECUTION=1

# execution command (with the number of parallel jobs)
EXEC_CMD="g++ -O3 -o patrol_blind.o ../../tools/patrol_blind.cpp; make exp2_all_outs -j 32"

echo -n "Generating models..."

for i in `seq -f %03g ${EXP_NUM}`; do  
    mkdir Mont_m_$i; \
    cd Mont_m_$i; \
    cp ../map_Montreal.kml map_Mont_m_$i.kml; \
    echo "PATH_TO_TOOLS := ../../tools" >Makefile; \
    echo "NUM_OF_EXECUTIONS := ${NUM_OF_EXECUTIONS}" >>Makefile; \
    echo "TRIALS_PER_EXECUTION := ${TRIALS_PER_EXECUTION}" >>Makefile; \
    echo "include ../../tools/Makefile" >>Makefile; \
    make blinit; \
    echo $EXEC_CMD >run.sh; \
    chmod u+x run.sh; \
    cd ..; \
done

echo "done"


