
# Comparison to Baseline - Experiment 1


This is a directory where our first experiment is performed.


results/  
   - a directory
   - it contains our automatically generated results:

       Exp1_graph.pdf
       Exp1_graph_no_labels.pdf
          - the graph included in the paper

       Exp1_one_graph.pdf
          - the graph included in the presentation

       Exp1_all_time_plots.pdf
          - the above graph but for all 4 algorithms

       Experiment1_report.pdf
          - a detailed summary of the obtained results
          - for each graph and algorithm, we analyze
            the obtained strategy values, number of iterations,
            and the execution times (in seconds, user time + system time)


Prepare_experiment_1.sh
   - a bash script 
   - generates pseudorandom graphs to subdirectories 
     Sub_g_[4-9]_n_0[1-5]/

Run_experiment_1.sh
   - a bash script 
   - runs the strategy computation (takes a bit longer time)

Summarize_experiment_1.sh
   - a bash script 
   - extracts data from files Sub_g_[4-9]_n_0[1-5]/*.out and
     Sub_g_[4-9]_n_0[1-5]/*.time, and creates the PDF files
     stored in ./results/


All the bash scripts uses Makefile and other programs from ../tools/.
