#! /bin/bash

# grid sizes
GRID_SIZES="4 5 6 7 8 9"

# number of graphs for each grid size
NUM_GRAPH=5

# number of executions for each graph
NUM_OF_EXECUTIONS=10

# number of trials in each execution
TRIALS_PER_EXECUTION=50

# ulimit bound - command ending with ";"
#ULIMIT=""
ULIMIT="ulimit -t 1600;"

# execution command (with the number of parallel jobs)
EXEC_CMD="make exp1_all_outs -j 32"

for g in ${GRID_SIZES}; do
    echo "Generating random subgraphs of a ${g}x${g} grid..."; \
    for n in `seq -f %02g ${NUM_GRAPH}`; do 
        mkdir Sub_g_${g}_n_$n; \
        cd Sub_g_${g}_n_$n; \
        python3 ../../tools/Square_generator.py -g ${g} -n 10 -s $n -o Sub_g_${g}_n_$n.kml; \
        echo "PATH_TO_TOOLS := ../../tools" >Makefile; \
        echo "NUM_OF_EXECUTIONS := ${NUM_OF_EXECUTIONS}" >>Makefile; \
        echo "TRIALS_PER_EXECUTION := ${TRIALS_PER_EXECUTION}" >>Makefile; \
        echo "ULIMIT := ${ULIMIT}" >>Makefile; \
        echo "include ../../tools/Makefile" >>Makefile; \
        make init; \
        echo $EXEC_CMD >run.sh; \
        chmod u+x run.sh; \
        cd ..; \
    done; \
    echo "done"; \
done

