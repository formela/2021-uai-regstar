#! /bin/bash

echo "Running for random subgraphs of grid..."

for i in  Sub_*; do
    cd $i; \
    echo $i; \
    ./run.sh; \
    cd ..; \
done

echo "done"


