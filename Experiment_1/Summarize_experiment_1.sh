#! /bin/bash

echo "Summarization of results for random subgraphs of a 5x5 grid..."

for i in  Sub_*; do
    cd $i; \
    echo $i; \
    make $i.ap.out $i.hp.out $i.ae.out $i.he.out; \
    make report; \
    cd ..; \
done

pdfjam --fitpaper true --rotateoversize true -o Experiment1_report.pdf */*report.pdf
echo "Joint report in Experiment1_report.pdf"
echo "done"

echo "Creating Time_data.csv ..."

echo "graph, g, n, algorithm, time" >Time_data.csv; \
# GRAPH_METHOD is '"g9_n1", 9, 1, "ae",' for Sub_g_9_n_01/Sub_g_9_n_01.ae.time

for i in  Sub_*/Sub*.time; do
    GRAPH_METHOD=`echo $i | sed "s/.*_g_\([^_]*\)_n_0\(.\)\.\([^.]*\)\..*/\"g\1_n\2\", \1, \2, \"\3\", /"`; \
    cat $i | bc | sed "s/^/${GRAPH_METHOD}/" >>Time_data.csv ; \
done

# echo "graph, g, n, method, time" >Time_ap_data.csv
# tail -n +2 Time_data.csv | grep ap >>Time_ap_data.csv

# echo "graph, g, n, method, time" >Time_hp_data.csv
# tail -n +2 Time_data.csv | grep hp >>Time_hp_data.csv

# echo "graph, g, n, method, time" >Time_ae_data.csv
# tail -n +2 Time_data.csv | grep ae >>Time_ae_data.csv

# echo "graph, g, n, method, time" >Time_he_data.csv
# tail -n +2 Time_data.csv | grep he >>Time_he_data.csv

echo "graph, g, n, algorithm, time" >Time_aphe_data.csv; \
for i in  Sub_*/Sub*.ap.time Sub_*/Sub*.he.time; do
    GRAPH_METHOD=`echo $i | sed "s/.*_g_\([^_]*\)_n_0\(.\)\.\([^.]*\)\..*/\"G\1(\2)\", \1, \2, \"\3\", /" | sed "s/ap/Baseline/" | sed "s/he/Regstar/" `; \
    cat $i | bc | sed "s/^/${GRAPH_METHOD}/" >>Time_aphe_data.csv ; \
done

echo "graph, g, n, algorithm, time" >Time_aphe_one_data.csv; \
for i in  Sub_*_n_03/Sub*.ap.time Sub_*_n_03/Sub*.he.time; do
    GRAPH_METHOD=`echo $i | sed "s/.*_g_\([^_]*\)_n_0\(.\)\.\([^.]*\)\..*/\"G\1\", \1, \2, \"\3\", /" | sed "s/ap/Baseline/" | sed "s/he/Regstar/" `; \
    cat $i | bc | sed "s/^/${GRAPH_METHOD}/" >>Time_aphe_one_data.csv ; \
done


# ggplot(data, aes(x=graph, y=time, ymax=1600, fill=method, stacked = FALSE,color=method)) + geom_dotplot(binwidth=0.01, method = "dotdensity", binaxis = "y", dotsize=1000, binpositions = "all") + facet_wrap(~method);
# ggplot(data, aes(x=graph, y=time, fill=method, color=factor(method))) + geom_boxplot() + facet_wrap(~method);

# ggplot(data, aes(x=g, y=time, ymax=800, fill=method, color=method)) + geom_jitter()

# ggplot(ap_data, aes(x=graph, y=time, ymax=1600, fill=method, stacked = FALSE,color=method,names=g)) + geom_dotplot(binwidth=0.01,   method = "dotdensity", binaxis = "y", dotsize=1000, binpositions = "all") + facet_wrap(~method);



Rscript -e "pdf(\"${PWD}/Exp1_all_time_plots.pdf\"); data<-read.csv(\"${PWD}/Time_data.csv\"); attach(data); library(ggplot2); ggplot(data, aes(x=graph, y=time, shape=algorithm, fill=algorithm, color=algorithm)) + geom_jitter() + coord_trans( y = \"log10\") + annotation_logticks(sides=\"l\",scaled = FALSE) + scale_shape_manual(values=c(2, 1, 3, 17)) + theme_bw() + theme(axis.text.x = element_text(angle = 90)) + labs(y = \"time (logarithmic scale)\") + scale_y_continuous(breaks = c(0,100,500,1000,1600)) + theme(legend.position = c(0.35, 0.10), legend.direction = \"horizontal\"); dev.off();" ;


echo "Common plot is in Exp1_all_time_plots.pdf"

Rscript -e "data<-read.csv(\"${PWD}/Time_aphe_data.csv\"); attach(data); library(ggplot2); ggplot(data, aes(x=graph, y=time, shape=algorithm, fill=algorithm, color=algorithm)) + geom_jitter() + coord_trans( y = \"log10\") + annotation_logticks(sides=\"l\",scaled = FALSE) + scale_shape_manual(values=c(1, 3, 17)) + theme_bw() + theme(axis.text.x = element_text(angle = 90)) + labs(y = \"time (logarithmic scale)\")  + scale_y_continuous(breaks = c(0,100,500,1000,1600)) +  theme(legend.position = c(0.35, 0.10), legend.direction = \"horizontal\"); ggsave(\"Exp1_graph.pdf\", width = 13, height = 9, units = \"cm\"); dev.off();" ;

Rscript -e "data<-read.csv(\"${PWD}/Time_aphe_data.csv\"); attach(data); library(ggplot2); ggplot(data, aes(x=graph, y=time, shape=algorithm, fill=algorithm, color=algorithm)) + geom_jitter() + coord_trans( y = \"log10\") + annotation_logticks(sides=\"l\",scaled = FALSE) + scale_shape_manual(values=c(1, 3, 17)) + theme_bw() + theme(axis.text.x = element_text(angle = 90))  + scale_y_continuous(breaks = c(0,100,500,1000,1600)) +  theme(axis.title.x=element_blank(), axis.title.y=element_blank(), legend.position = c(0.35, 0.10), legend.direction = \"horizontal\"); ggsave(\"Exp1_graph_no_labels.pdf\", width = 13, height = 9, units = \"cm\"); dev.off();" ;

Rscript -e "data<-read.csv(\"${PWD}/Time_aphe_one_data.csv\"); attach(data); library(ggplot2); ggplot(data, aes(x=graph, y=time, shape=algorithm, fill=algorithm, color=algorithm)) + geom_jitter() + coord_trans( y = \"log10\") + annotation_logticks(sides=\"l\",scaled = FALSE) + scale_shape_manual(values=c(1, 3, 17)) + theme_bw() + labs(y = \"time (logarithmic scale)\")  + scale_y_continuous(breaks = c(0,100,500,1000,1600)) +  theme(legend.position = c(0.75, 0.70), legend.direction = \"vertical\"); ggsave(\"Exp1_one_graph.pdf\", width = 13, height = 9, units = \"cm\"); dev.off();" ;


echo "Produced \"Exp1_graph.pdf\" and \"Exp1_graph_no_labels.pdf\" ."

echo "done"
