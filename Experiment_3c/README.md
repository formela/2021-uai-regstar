
# Effect of increasing number of memeory elements


This is a directory where our third experiment is performed on a building with three floors.


results/
   - a directory
   - it contains our automatically generated results:
       Experiment3c_report.pdf
          - a detailed summary of the obtained results
          - for each number of memory elements, we analyze
            the obtained strategy values, number of iterations,
            and the execution times (in seconds, user time + system time)
       Table2c.txt
          - source data for Table 2


Template3x_building_r10_c4.blin
   - Plan of a three-floor building with 4 nodes representing a corridor
     and 10 nodes representing rooms (possible targets with blindness 0.1,
     target value 100, and attack time 300) in each floor. The floors
     are connected by two stairs (of length 10) between pairs of the marginal
     corridor nodes.
   - the structure is:
                    R       R       R       R
                    |       |       |       |
                    5       5       5       5
                    |       |       |       |
            R - 5 - C - 2 - C - 2 - C - 2 - C - 5 - R
                  / |       |       |       | \
                 /  5       5       5       5  \
                /   |       |       |       |   \
               /    R       R       R       R    \
              /                                   \
             10                                   10
              \                                   /
               \    R       R       R       R    /
                \   |       |       |       |   /
                 \  5       5       5       5  /
                  \ |       |       |       | /
            R - 5 - C - 2 - C - 2 - C - 2 - C - 5 - R
                  / |       |       |       | \
                 /  5       5       5       5  \
                /   |       |       |       |   \
               /    R       R       R       R    \
              /                                   \
             10                                   10
              \                                   /
               \    R       R       R       R    /
                \   |       |       |       |   /
                 \  5       5       5       5  /
                  \ |       |       |       | /
            R - 5 - C - 2 - C - 2 - C - 2 - C - 5 - R
                    |       |       |       |
                    5       5       5       5
                    |       |       |       |
                    R       R       R       R

   - each edge with length 2 stands for walking,
     the edges with length 5 represent unlocking doors
     and entering the particular rooms, the two edges
     of length 10 model stairs between the floors.


Prepare_experiment_3.sh
   - a bash script
   - generates input graphs with 1..8 memory elements to subdirectories
     Building_m_00[1-8]/

Run_experiment_3.sh
   - a bash script
   - runs the strategy computation (takes a bit longer time)

Summarize_experiment_3.sh
   - a bash script
   - extracts data from files Building_m_00[1-8]/*.out
     Building_m_00[1-8]/*.time, and creates the PDF file
     Experiment3c_report.pdf and Table2c.txt stored in ./results/


All the bash scripts uses Makefile and other programs from ../tools/.
