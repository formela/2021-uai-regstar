#! /bin/bash

# number of experiments to be run (to what number of memory elements)
EXP_NUM=8

# number of executions in each experiment
NUM_OF_EXECUTIONS=200

# number of trials in each execution
TRIALS_PER_EXECUTION=1

# Building plan
BUILDING=Template3x_building_r10_c4.blin

# execution command (with the number of parallel jobs)
EXEC_CMD="make exp2_all_outs -j 12"

echo -n "Generating models of ${BUILDING}..."

for i in `seq -f %03g ${EXP_NUM}`; do  
    mkdir Building_m_$i; \
    cp ${BUILDING} Building_m_$i/Building_m_$i.blin; \
    echo "5 ${i}" >> Building_m_$i/Building_m_$i.blin; \
    cd Building_m_$i; \
    echo "PATH_TO_TOOLS := ../../tools" >Makefile; \
    echo "NUM_OF_EXECUTIONS := ${NUM_OF_EXECUTIONS}" >>Makefile; \
    echo "TRIALS_PER_EXECUTION := ${TRIALS_PER_EXECUTION}" >>Makefile; \
    echo "SOURCE_NAME := Building_m_${i}" >>Makefile; \
    echo "include ../../tools/Makefile" >>Makefile; \
    make blinit; \
    echo $EXEC_CMD >run.sh; \
    chmod u+x run.sh; \
    cd ..; \
done

echo "done"


