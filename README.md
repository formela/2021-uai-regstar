# Regstar: efficient strategy synthesis for adversarial patrolling games (UAI 2021)


Implementation of an efficient strategy synthesis method applicable to adversarial patrolling problems on graphs with arbitrary-length edges and possibly imperfect intrusion detection. 

Published in 

*David Klaška, Antonín Kučera, Vít Musil, Vojtěch Řehák. Regstar: efficient strategy synthesis for adversarial patrolling games. Proceedings of the Thirty-Seventh Conference on Uncertainty in Artificial Intelligence, PMLR 161:471-481, 2021.*

https://proceedings.mlr.press/v161/klaska21a.html

    @inproceedings{DBLP:conf/uai/Klaska0MR21,
      author    = {David Kla{\v{s}}ka and Anton{\'{i}}n Ku{\v{c}}era and V{\'{i}}t Musil and Vojt\v{e}ch {\v{R}}eh{\'{a}}k},
      editor    = {Cassio P. de Campos and Marloes H. Maathuis and Erik Quaeghebeur},
      title     = {Regstar: efficient strategy synthesis for adversarial patrolling games},
      booktitle = {Proceedings of the Thirty-Seventh Conference on Uncertainty in Artificial
                   Intelligence, {UAI} 2021, Virtual Event, 27-30 July 2021},
      series    = {Proceedings of Machine Learning Research},
      volume    = {161},
      pages     = {471--481},
      publisher = {{AUAI} Press},
      year      = {2021},
      url       = {https://proceedings.mlr.press/v161/klaska21a.html},
      biburl    = {https://dblp.org/rec/conf/uai/Klaska0MR21.bib},
    }

Up-to-date version of Regstar is at [gitlab.fi.muni.cz/formela/regstar](https://gitlab.fi.muni.cz/formela/regstar/).

# Supplementary materials


Experiment_1/
   - Comparison to Baseline described in Section 5.1
   - for more detail see ./Experiment_1/README.md

Experiment_2/
   - Real-World Patrolling Graph described in Section 5.2
   - for more detail see ./Experiment_2/README.md

Experiment_3a/
   - Patrolling an Office Building with one floor
     described in Section 5.3 (Table 2)
   - for more detail see ./Experiment_3a/README.md

Experiment_3b/
   - Patrolling an Office Building with two floors
     described in Section 5.3 (Table 2)
   - for more detail see ./Experiment_3b/README.md

Experiment_3c/
   - Patrolling an Office Building with three floors
     described in Section 5.3 (Table 2)
   - for more detail see ./Experiment_3c/README.md

Experiment_3d/
   - Patrolling an Office Building with one floor
     described in Section 5.3 (Table 3)
   - for more detail see ./Experiment_3d/README.md


tools/
   - implementations of the presented patrolling algorithms
   - Makefile and other supplementary script
   - for more detail see ./tools/README.md

README.md
   - this file

SW_compatibility_and_versions.txt
   - self-explanatory
