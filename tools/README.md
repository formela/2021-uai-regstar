

# Directory with the main programs and scripts


patrol_auxvert_pivotgrad.cpp
   - C++ implementation of the patrolling algorithm with
        - auxiliary vertices for long edges and
        - pivot-based gradient computation
   - this is referred to as "Baseline" in the paper
   - used in Experiment 1

patrol_auxvert_exactgrad.cpp
   - C++ implementation of the patrolling algorithm with
        - auxiliary vertices for long edges and
        - exact gradient computation
   - used in Experiment 1

patrol_heap_pivotgrad.cpp
   - C++ implementation of the patrolling algorithm with
        - heap for long edges and
        - pivot-based gradient computation
   - used in Experiment 1

patrol_heap_exactgrad.cpp
   - C++ implementation of the patrolling algorithm with
        - heap for long edges and
        - exact gradient computation
   - this is referred to as "Regstar" in the paper
   - used in Experiment 1


patrol_blind.cpp
   - C++ implementation of the patrolling algorithm with
        - heap for long edges
        - exact gradient computation
        - imperfect intrusion detection
        - optimized for large graphs
   - used in Experiments 2 and 3


Makefile
   - instruction file for `make`
   - used in all Experiments 1, 2, and 3


Square_generator.py
   - a Python 3 script
   - generates pseudorandom graph based on selected vertices of a grid
   - output is an input graph for patrolling
   - see `python3 Square_generator.py -h` for more detail
   - used in Experiment 1

kml2in.py
   - a Python 3 script
   - transforms map points from .kml file to an input graph for patrolling
   - see `python3 kml2in.py -h` for more detail
   - used in all Experiments 1, 2, and 3


Report_template.tex
   - a LaTeX template for PDF with results obtained during Experiment 1

BlReport_template.tex
   - a LaTeX template for PDF with results obtained during Experiments 2 and 3


test_g++_rand_generator.cpp
   - a C++ program testing compatibility of a pseudorandom generator
   - ends with exit(x), where x>0 for an incompatible generator

test_python_rand_generator.py
   - a Pyton 3 program testing compatibility of a pseudorandom generator
   - ends with exit(x), where x>0 for an incompatible generator
