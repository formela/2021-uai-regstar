#!/usr/bin/env python3

from xml.dom import minidom
import random
import math
import sys, getopt, re
from geopy.distance import distance
from geopy.distance import geodesic


#######################
# parameters

# default setting
inputfile = None
outputfile = None
TaxiCab = True
CostByName = False
DistanceUnit = None
rseed = 13
MemElements = 4
Blind = False

HelpInfo = """Options:
   -i <inputfile>, --ifile <inputfile>
          name of the *.kml input file (NECESSARY)

   -o <outputfile>, --ofile <outputfile>
          output file name, (default = *.in version of the inputfile)

   -e
          distances computed in Euclidian metric
          (taxicab metric is default)

   --distunit=UNIT
          distances transformed to UNIT = { meter, 100m, km, mile }
          (Default value is no transforamtion.)

   -s <number>
          seed for random generator

   -m <number>
          number of memory elements assigned to each node (default is 4)

   -b
          add specification for blindness (randomly assigned)

   -c
          costs assigned from names
          (random assignment is default)"""


try:
    opts, args = getopt.getopt(sys.argv[1:],"bm:es:chi:o:",["ifile=","ofile=","distunit="])
except getopt.GetoptError:
    print(HelpInfo)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print(HelpInfo)
        sys.exit()
    elif opt == '-e':
        TaxiCab = False
    elif opt in ("-s"):
        rseed = int(arg)
    elif opt == '-b':
        Blind = True
    elif opt in ("-m"):
        MemElements = int(arg)
    elif opt == '-c':
        CostByName = True
    elif opt in ("-i", "--ifile"):
        inputfile = arg
    elif opt in ("-o", "--ofile"):
        outputfile = arg
    elif opt in ("--distunit"):
        if arg in { "meter", "100m", "km", "mile" }:
            DistanceUnit = arg
        else:
            print("Warning: Wrong --distunit value! No transformation will be applied.")



# default values
if inputfile is None:
    print("Error: Missing input file argument!")
    print(HelpInfo)
    sys.exit(2)


if outputfile is None:
    outputfile = re.sub(r'\.kml$', '.in', inputfile)

random.seed(rseed)

# print ('Input file is ', inputfile)
# print ('Output file is ', outputfile)
# print ('DistanceUnit is ', DistanceUnit)


#######################
# Data types

class Node:
    def __init__(self,
                 coord_x: float, coord_y: float
                 # cost: float, attack_time: float
                 ) -> None:
        self.coord_x: float = coord_x
        self.coord_y: float = coord_y
        self.dists: List[float] = []
        # self.cost: float = cost
        # self.atack_time: float = atack_time


#######################
# Aux functions

def Distance(FromVert: Node, ToVert: Node) -> float:
    if TaxiCab:
        # taxicab distance
        if DistanceUnit is None:
            return abs(FromVert.coord_x-ToVert.coord_x) + abs(FromVert.coord_y-ToVert.coord_y)
        d = distance((FromVert.coord_x, FromVert.coord_y), (ToVert.coord_x, FromVert.coord_y)) + \
            distance((FromVert.coord_x, FromVert.coord_y), (FromVert.coord_x, ToVert.coord_y))
    else:
        # Euclidean distance
        if DistanceUnit is None:
            return math.sqrt((FromVert.coord_x-ToVert.coord_x)**2 + (FromVert.coord_y-ToVert.coord_y)**2)
        d = distance((FromVert.coord_x, FromVert.coord_y), (ToVert.coord_x, TomVert.coord_y))
    # units
    if DistanceUnit == "meter":
        return d.meters
    if DistanceUnit == "100m":
        return math.ceil(d.meters/100)
        #return math.floor(d.meters/100 + 0.5)*100
    if DistanceUnit == "km":
        return math.ceil(d.meters/1000)
        #return math.floor(d.meters/1000 + 0.5)*1000
    return d.miles


#######################
# XML Parsing

# parse an xml file by name
mydoc = minidom.parse(inputfile)

# get coordinates
coordinates = mydoc.getElementsByTagName('coordinates')
# get names (the fist one is the Document name)
names = mydoc.getElementsByTagName('name')[1:]


if len(names) != len(coordinates):
    print("Different number of name elements {} and coordinates {}.".
          format(len(names), len(coordinates)))
    sys.exit(2)

# values from names
if CostByName:
    ValuesFromNames = [float(re.sub('[^0-9.]', '', x.firstChild.data)) for x in names]


vertices = []

# convert to numbers and create Nodes
for elem in coordinates:
    (x, y, z) = elem.firstChild.data.strip().split(',')
    vertices.append(Node(float(x), float(y)))


# compute transition matrix
for FromVert in vertices:
    for ToVert in vertices:
        FromVert.dists.append(Distance(FromVert, ToVert))


# edge-length statistics
NumberOfNodes = len(vertices)
MaxEdge = 0
MeanEdge = 0
Mean_X = 0
Mean_Y = 0
for FromVert in vertices:
    MaxEdge = max(MaxEdge, max(FromVert.dists))
    MeanEdge += sum(FromVert.dists)
    Mean_X += FromVert.coord_x
    Mean_Y += FromVert.coord_y
MeanEdge /= NumberOfNodes * (NumberOfNodes-1)
Mean_X /= NumberOfNodes
Mean_Y /= NumberOfNodes
# print(MaxEdge, MeanEdge, Mean_X, Mean_Y)

# open the file for export
with open(outputfile, "w") as file:

    file.write("# <number of graph states>\n")
    file.write(f"{NumberOfNodes}\n")

    file.write("# <number of targets>\n")
    file.write(f"{NumberOfNodes}\n")

    file.write("# <vector of attack times for targets>\n")
    #attack_times = [random.randint(int(2*MaxEdge), int(2*MaxEdge + MeanEdge)) for i in range(NumberOfNodes)]
    if TaxiCab and DistanceUnit is None:
    #if TaxiCab:
        attack_times = [int(MaxEdge+MeanEdge+3)]*NumberOfNodes
    else:
        attack_times = [int(2*MaxEdge+MeanEdge)]*NumberOfNodes
    file.write(" ".join(map(str, attack_times))+"\n")

    file.write("# <vector of costs for targets>\n")
    if CostByName:
        costs = [(i+5)*100 for i in ValuesFromNames]
    else:
        costs = [random.randint(180, 200) for i in range(NumberOfNodes)]
    file.write(" ".join(map(str, costs))+"\n")

    if Blind:
        file.write("# <vector of blindness probabilities>\n")
        blinds = [random.randint(0, 20)/100 for i in range(NumberOfNodes)]
        file.write(" ".join(map(str, blinds))+"\n")

    file.write("# <transition matrix>\n")
    for FromVert in vertices:
        printlist = ["-1" if x == 0 else "%.0f" % x for x in FromVert.dists]  # 0 decimal digits
        print(*printlist, file=file)

    file.write("# <memory elements, e.g., 5 4 - each 4>\n")
    file.write("5 {}".format(MemElements))
