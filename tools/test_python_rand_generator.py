#!/usr/bin/env python3

import random

random.seed(1)
pool = [(x+1)*10+(y+1) for x in range(10) for y in range(10)]
random.shuffle(pool)

r = [64, 48, 76, 62, 15, 31, 49, 20, 21, 92, 55, 47, 95, 61, 107, 101, 77, 27, 91, 44, 35, 63, 102, 110, 75, 16, 69, 87, 50, 90, 34, 105, 41, 84, 36, 58, 42, 56, 30, 98, 53, 79, 106, 32, 18, 78, 57, 93, 22, 17, 52, 97, 99, 81, 29, 89, 82, 70, 54, 72, 33, 25, 46, 104, 67, 39, 109, 65, 38, 100, 12, 80, 85, 13, 96, 51, 24, 86, 40, 45, 103, 11, 88, 66, 60, 14, 73, 23, 37, 59, 94, 71, 68, 74, 26, 43, 19, 108, 83, 28]

if pool != r:
    print(f"Error: Different random generator - random.shuffle() differs.")
    sys.exit(2)

random.seed(13)

if 188 != random.randint(180, 200) or 189 != random.randint(180, 200) or 185 != random.randint(180, 200):
    print(f"Error: Different random generator - random.randint() differs.")
    sys.exit(3)

print("OK")

