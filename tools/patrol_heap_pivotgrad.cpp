#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <vector>
#include <algorithm>


#define UNIT_EDGES 0
//0 ... edges have arbitrary lengths
//1 ... edges have unit lengths
//2 ... the input graph may have arbitrarily long edges and auxiliary
//	vertices are introduced to reduce the graph to unit edges

#define PATH_MERGE_THRESHOLD 1
//(meaningful only if UNIT_EDGES is 0)
//paths whose lengths differ by less than this are considered equally long
//if you set this to 0, then no merging will happen at all, leading to
//exponentially many paths examined even for graphs with unit edges

#define VISITING_BONUS 1e-6
//(meaningful only if UNIT_EDGES is not 1)
//if set to a small positive value (e.g. 1e-6), then every edge which is dominated
//by a path of at most the same length is deleted; more generally,
//this constant determines the distance bonus for visiting a vertex

#define REPLACEMENT_EDGES 1
//0 ... computing the gradient exactly (using x/(x+y+z) etc.)
//1 ... substituting the last edge variable (1-(x+y) instead of z etc.)

#define BLINDNESS 0
//0 ... upon visiting a target, the attacker is detected surely
//1 ... each target is parametrized by the probability of detecting an ongoing attack

#define SEED 23
//0 ... use a random seed (the behavior of the program for different executions may vary)
//>0 ... use SEED (the behavior of the program is the same for each execution)
//can be overridden by the first command-line parameter

#define REPORT_INPUT 1
//whether to report the loaded input (just for a check)

#define SUCC_LIST_OUTPUT 1
//0 ... report a strategy as the matrix of transition probabilities
//1 ... report a strategy as the list of successors (with non-zero probabilities) of each state

#define N_TRIALS 10000
//0 ... run the program in an infinite loop
//>0 ... make N_TRIALS trials (initial starting points)

#define REPORT_ITERATIONS 0
//0 ... do not report any results during the iterations of the gradient ascent
//1 ... report the current strategy after each iteration (the user is prompted to continue)

#define REPORT_TRIALS 2
//0 ... never report any intermediate results
//1 ... report only temporarily best as soon as it is found
//2 ... report only the values of the synthesized strategies
//3 ... report the strategy synthesized in each trial

#define TRIAL_REPORT_PERIOD 0
//0 ... never report intermediate results
//>0 ... report the temporarily best result every TRIAL_REPORT_PERIOD trials

#define REPORT_SHORT_STRATEGY 1 //whether to report a strategy in a format
//which can later be loaded as a seed

#define INITIAL_DC .1 //the initial value of the distance coefficient

#define DC_MULT_FACTOR .99 //the multiplication factor used in every iteration

#define DC_RETURN_MULT_FACTOR .8 //the multiplication factor used when
//the new point is too far so that it is worse than the previous one

#define N_RETURNS_THRESHOLD 10 //after how many unsuccessful attempts at
//finding a better strategy to disable another edge

#define ITERATION_THRESHOLD .001 //if the value of the strategy in the
//next iteration is not better by at least this threshold,
//then another edge gets disabled

#define IMPORTANCE_THRESHOLD .01 //how close to the minimum the probability
//of catching an attack must be in order to be considered

#define RELATIVE_THRESHOLDS 1 //if turned on, the two thresholds above
//are multiplied by the maximum weight of a target

#define EDGE_DISABLING_THRESHOLD .01 //how low the probability of using
//an edge must be in order to disable it

#define DIVIDE_EDGE_DISABLING_THRESHOLD 1 //if turned on, the above
//threshold is divided by the out-degrees of the individual vertices

#define DISABLE_LONG_EDGES 0 //if turned on, then long problematic edges
//get disabled from time to time - and always when the achived protection
//is zero (meaningful only if UNIT_EDGES is off)


#define MAXN 60 //upper bound for the number of vertices
#define MAXQ 240 //upper bound for the number of states
#define MAXG 32 //upper bound for the number of targets
#define MAXD 32 //upper bound for the maximum length of attack (meaningful only if UNIT_EDGES is on)



//input specification:
//n g
//Time[0..g-1]
//Weight[0..g-1]
//if BLINDESS is on: Blindess[0..g-1]
//  UNIT_EDGES == 1: IsEdge[0..n-1][0..n-1]
//  UNIT_EDGES != 1: EdgeLength[0..n-1][0..n-1], negative length for no edge
//memory allocation specification


#if UNIT_EDGES
typedef int TIME_TYPE;
#else
typedef double TIME_TYPE;
#endif


int n; //number of vertices
int q; //number of states
int g; //number of targets
int type; //type of assigning memory sizes
          //(0 = manually, 1 = at random, 2 = by degrees, /*3 = by connectedness, 4 = gradual incrementation*/ 5 = to all)
int qTotal; //desired number of states
TIME_TYPE Time[MAXG]; //Time[i] is the time needed to perform a successful attack at vertex i
double Weight[MAXG]; //Weight[i] is the value of target i
double MaxWeight; //maximum of Weight[i] over all targets i
double OrigMaxWeight = 1.; //in case the above gets normalized to 1.
int tMaxWeight; //index of the most valued target
int MemorySize[MAXN]; //MemorySize[i] is the memory size of vertex i

#if UNIT_EDGES
int VIsEdge[MAXN][MAXN]; //VIsEdge[i][j] says whether there is an edge from vertex i to vertex j
#endif

#if UNIT_EDGES != 1
#define INFTY ((double) (1ULL << 63))
double VEdgeLength[MAXN][MAXN]; //INFTY = no edge
#endif

#if BLINDNESS
double Blindness[MAXG]; //Blindess[t] is the probability of NOT detecting an ongoing attack in t when visiting t
#endif

struct ADJ
//information about an adjacent vertex in a predecessor/successor list
{
	int To;
#if !(UNIT_EDGES)
	double Length;
#endif
};

int VInDegree[MAXN]; //VInDegree[i] is the number of predecessors of vertex i
ADJ VPred[MAXN][MAXN]; //VPred[i][j] is the j-th predecessor of vertex i
int VOutDegree[MAXN]; //VOutDegree[i] is the number of successors of vertex i
ADJ VSucc[MAXN][MAXN]; //VSucc[i][j] is the j-th successor of vertex i

int Q2V[MAXQ]; //Q2V[i] is the vertex to which state i corresponds
int V2Q[MAXN]; //V2Q[i] is the first state which corresponds to vertex i
int GlobOutDegree[MAXQ]; //GlobOutDegree[i] is the number of successors of state i
ADJ GlobSucc[MAXQ][MAXQ]; //GlobSucc[i][j] is the j-th successor of state i

int IsReachable[MAXQ]; //whether each state is reachable
int Q[MAXQ], QBeg, QEnd;

int IVal; //whether computing IVal

int nPDCalls;
int nIterations;



struct STRATEGY
//structure describing a strategy (note that during the
//gradient ascent, the edges become disabled one by one)
{
	int OutDegree[MAXQ]; //OutDegree[i] is the number of successors of state i
	ADJ Succ[MAXQ][MAXQ]; //Succ[i][j] is the j-th successor of state i
	double Prob[MAXQ][MAXQ]; //Prob[i][j] is the probability of going to state j when being at state i
	double Value; //the computed lower bound on the value of the strategy
};

STRATEGY S; //the current strategy
STRATEGY PrevS; //the strategy used in the previous iteration of the gradient ascent
STRATEGY BestS; //the best strategy encountered in the current trial
STRATEGY AbsBestS; //the best strategy encountered during the entire execution of the program

#if UNIT_EDGES
double R[MAXG][MAXQ][MAXD];
	//in the edge-attacks setting, R[t][i][d] is the probability that the defender,
	//provided he is currently at i, gets to a state corresponding to t
	//for the first time (not counting the initial i) in exactly d+1 steps;
	//in the vertex-attacks setting, R[t][i][d] is the probability that the defender,
	//provided he is currently at i, gets to a state corresponding to t
	//for the first time (counting the initial i) in exactly d steps;
double PD[MAXQ][MAXD];
//PD[i][d] is the partial derivative of R[t][i][d] with respect to S.Prob[a][b]
//for the currently examined target t and edge (a,b)
#else
struct HEAPITEM
{
	double d; //distance towards the examined target
	double p; //probability of the corresponding path(s)
	int i; //initial state
	int id; //id of this heap-item
	
	bool operator < (const HEAPITEM& H) const
	{
		return this->d > H.d;
	}
};

std::vector<HEAPITEM> Heap;
std::vector<HEAPITEM> HeapList[MAXG];
std::vector<int> LabelO2N[MAXG];
int nLabels[MAXG];
std::vector<std::pair<int, int> > Contrib[MAXG];
std::vector<double> PD; //the index is the same as into HeapList[t] for the current t
#endif

double Catch[MAXG][MAXQ]; //Catch[t][i] is the probability of covering an attack at target t when being at state i,
	//i.e., the sum of R[t][i][d] over d's
double Loss[MAXG][MAXQ]; //Loss[t][i] is the loss (non-positive) due to an attack at target t when being at state i
double MinLoss[MAXG]; //MinLoss[t] is the minimum of Loss[t][i] over i's (this is used to save time
        //by not computing PD for those targets t which are always (from all i's) well-covered/valueless)
double TotalPD[MAXQ]; //TotalPD[i] is the sum of PD[i][d] over d's
double Diff[MAXQ][MAXQ]; //differences in S.Prob computed by the gradient ascent
	//(S.Prob must not be updated directly because it is used in order to compute PD
	//for other potential targets t)
double Imp[MAXQ]; //Imp[i] is the importance (closeness to minimum) of Loss[t][i]
	//for the currently examined target t


//function declarations
void Assert(bool MustBeTrue, const char* s, int a);
template<typename T> void Load(T& a, const char* format);
void LoadGraph();
void LoadType();
void LoadStrategy(STRATEGY& S);
void FloydWarshall(double VDist[MAXN][MAXN], double bonus);
void Unfold(int i, int& v, int& e);
char* StateName(int i, char* Buffer = NULL);
char VertexName(int v);
void PrintStrategy(STRATEGY& S);
void PrintValue(double v);
void PrintShortStrategy(STRATEGY& S);
void PrintInput();
void ToUnitEdges();
void InitVSucc(bool Check);
void SetMemorySizes();
void InitFM();
void InitStrategySucc(STRATEGY& S);
void InitStartingPoint(STRATEGY& S);
void FindReachableStates(STRATEGY& S, int* IsReachable);
double GetBlindness(int t);
void ComputeR(STRATEGY& S);
void ComputePD(int t, int a, int b);
void Minimize(double& a, double b);
void Maximize(double& a, double b);
double EvaluateStrategy(STRATEGY& S);
void Normalize(STRATEGY& S, int i);
void NormalizeAll(STRATEGY& S);
void DisableEdge(STRATEGY& S, int i, int k);
bool DisableLeastUsedEdge();
bool DisableLongEdge();
bool DisableSomeEdge(int trial);
void Ascend(int trial);
void ComputeSwitchingR();


//function definitions
void Assert(bool MustBeTrue, const char* s, int a)
{
	if(!(MustBeTrue))
	{
		fprintf(stderr, "Too low predefined constant %s! Set it to at least %d.\n", s, a);
		exit(0);
	}
}


const char* FORMAT_STRING(int& a)
{
	return "%d";
}


const char* FORMAT_STRING(double& a)
{
	return "%lf";
}


char LoadVarNameBuffer[64];
template<typename T> void Load(T& a)
//load a value to $a from the standard input
{
	static char buffer[64];
	
AGAIN:
	if(scanf("%60s", buffer) < 1)
	{
		fprintf(stderr, "Ran out of tokens when loading variable \"%s\"!\n", LoadVarNameBuffer);
		exit(0);
	}
	
	if(buffer[0] == '#')
	{
		while(getchar() != '\n');
		goto AGAIN;
	}
	else if(sscanf(buffer, FORMAT_STRING(a), &a) < 1)
	{
		fprintf(stderr, "Cannot parse token \"%s\" when loading variable \"%s\"!\n", buffer, LoadVarNameBuffer);
		exit(0);
	}
}


void LoadGraph()
//loads the graph of the patrolling game
{
	sprintf(LoadVarNameBuffer, "n");
	Load(n);
	Assert(n <= MAXN, "MAXN", n);
	
	sprintf(LoadVarNameBuffer, "g");
	Load(g);
	Assert(g <= MAXG, "MAXG", g);
	assert(g <= n);

	for(int i = 0; i < g; i++)
	{
		sprintf(LoadVarNameBuffer, "Time[%d]", i);
		Load(Time[i]);

        #if UNIT_EDGES
		Assert(Time[i] <= MAXD, "MAXD", Time[i]);
        #endif
	}

	for(int i = 0; i < g; i++)
	{
		sprintf(LoadVarNameBuffer, "Weight[%d]", i);
		Load(Weight[i]);
		assert(Weight[i] >= 0.);
	}

#if BLINDNESS
	for(int i = 0; i < g; i++)
	{
		sprintf(LoadVarNameBuffer, "Blindness[%d]", i);
		Load(Blindness[i]);
		assert(Blindness[i] >= 0.);
		assert(Blindness[i] <= 1.);
	}
#endif
	
#if UNIT_EDGES == 1
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			sprintf(LoadVarNameBuffer, "VIsEdge[%d][%d]", i, j);
			Load(VIsEdge[i][j]);
			assert((VIsEdge[i][j] == 0) || (VIsEdge[i][j] == 1));
		}
	}
#else
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			double& a = VEdgeLength[i][j];
			sprintf(LoadVarNameBuffer, "VEdgeLength[%d][%d]", i, j);
			Load(a);

			if(0)
			//taking 0 as non-existence
			{
				if(a == 0.)
				{
					a = INFTY;
				}
			}

			if(a < 0.)
			{
				a = INFTY;
			}
		}
	}
#endif
	
}


void LoadType()
//loads the memory allocation type
{
	sprintf(LoadVarNameBuffer, "type");
	Load(type);
	assert(type >= 0);
	assert(type <= 5);

	if(0) printf("type: %d\n", type);
	
	if(type)
	//determining the memory sizes automatically
	{
		sprintf(LoadVarNameBuffer, "qTotal");
		Load(qTotal);
		
		if(type == 5)
		{
			qTotal *= n;
		}

		assert(qTotal >= n);
		Assert(qTotal <= MAXQ, "MAXQ", qTotal);
	}
	else
	//the memory sizes are assigned manually
	{
		q = 0;
		for(int i = 0; i < n; i++)
		{
			sprintf(LoadVarNameBuffer, "MemorySize[%d]", i);
			Load(MemorySize[i]);
			q += MemorySize[i];
		}

		Assert(q <= MAXQ, "MAXQ", q);
	}
}


void LoadStrategy(STRATEGY& S)
{
	InitStrategySucc(S);
	
	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			sprintf(LoadVarNameBuffer, "S.Prob[%d][%d]", i, j);
			Load(S.Prob[i][j]);
			assert(S.Prob[i][j] >= 0.);
			assert(S.Prob[i][j] <= 1.);

                #if UNIT_EDGES
			if(!(VIsEdge[Q2V[i]][Q2V[j]]))
                #else
			if(VEdgeLength[Q2V[i]][Q2V[j]] == INFTY)
                #endif
			{
				assert(S.Prob[i][j] == 0.);
			}
		}
	}

	if(0) PrintStrategy(S);
	
	for(int i = 0; i < q; i++)
	{
		for(int k = 0; k < S.OutDegree[i]; k++)
		{
			int j = S.Succ[i][k].To;
			if(S.Prob[i][j] <= 0.)
			{
				if(0) printf("Disabling edge %d->%d (successor #%d of %d)\n", i, j, k, S.OutDegree[i]);
				DisableEdge(S, i, k);
				k--;
			}
		}
		
		assert(S.OutDegree[i] > 0);
	}
	
	NormalizeAll(S);
}


#if UNIT_EDGES != 1
void FloydWarshall(double VDist[MAXN][MAXN], double bonus)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			VDist[i][j] = VEdgeLength[i][j] - bonus;
		}

		Minimize(VDist[i][i], 0.);
	}

	for(int k = 0; k < n; k++)
	//Floyd-Warshall
	{
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				Minimize(VDist[i][j], VDist[i][k] + VDist[k][j]);
			}
		}
	}
}
#endif


void Unfold(int i, int& v, int& e)
//unfolds state i to the corresponding vertex and memory element
{
	v = Q2V[i];
	int diff = i - V2Q[v];
	e = diff % MemorySize[v];
}


char AuxBuffer[64]; //for printing a second state name in one printf
char* StateName(int i, char* Buffer)
//fills buffer with the name of state i (vertex name, memory element);
//the second parameter defaults to NULL and is set to the local buffer then
{
	static char buffer[64];
	if(!(Buffer))
	{
		Buffer = buffer;
	}
	
	int v, e;
	Unfold(i, v, e);

	sprintf(Buffer, "%c%d", VertexName(v), e + 1);

	return Buffer;
}


char VertexName(int v)
{
	return v + 'A';
}


void PrintStrategy(STRATEGY& S)
//prints a strategy to the standard output
{
	int LocIsReachable[MAXQ];
	FindReachableStates(S, LocIsReachable);

#if SUCC_LIST_OUTPUT
	for(int i = 0; i < q; i++)
	{
		if(!(LocIsReachable[i]))
		{
			printf("(UNREACHABLE) ");
		}
		
		printf("%s:", StateName(i));
		for(int j = 0; j < q; j++)
		{
			if(S.Prob[i][j] > 0.)
			{
				printf("%7.3lf->%s", S.Prob[i][j], StateName(j));
			}
		}
		printf("\n");
	}
#else
	printf("%7s", "");
	for(int i = 0; i < q; i++)
	{
		printf("%7s", StateName(i));
	}
	printf("\n");

	for(int i = 0; i < q; i++)
	{
		printf("%7s", StateName(i));
		for(int j = 0; j < q; j++)
		{
			printf("%7.3lf", S.Prob[i][j]);
		}

		printf("\n");
	}
#endif

#if REPORT_SHORT_STRATEGY
	PrintShortStrategy(S);
#endif

	PrintValue(S.Value);
}


void PrintValue(double v)
{
	printf("Value: %7.3lf (%7.3lf %%)\n\n", (v + 1.) * OrigMaxWeight, ((v / MaxWeight) + 1.) * 1e2);
}


void PrintShortStrategy(STRATEGY& S)
//prints a strategy to the standard output in a format which can later be loaded as a seed
{
	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			printf("% .6lf", S.Prob[i][j]);
		}

		printf("\n");
	}
}


void PrintInput()
{
	printf("Targets: %c..%c\n", 'A', (g - 1) + 'A');
	for(int i = 0; i < n; i++)
	{
#if UNIT_EDGES
#define TIME_STR "%d"
#else
#define TIME_STR "%.3lf"
#endif

		printf("%c (MS %d",  i + 'A', MemorySize[i]);

		if(i < g)
		{
			printf("; AT " TIME_STR "; W %.3lf", Time[i], Weight[i]);
		}

        #if BLINDNESS
		printf("; BN %.3lf", Blindness[i]);
        #endif
		printf("): ");
		
		for(int j = 0; j < VOutDegree[i]; j++)
		{
			printf("%c ", VSucc[i][j].To + 'A');
                #if !(UNIT_EDGES)
			printf("(%.3lf) ", VSucc[i][j].Length);
		#endif
		}
		
		printf("\n");
	}
}


void ToUnitEdges()
{
#if UNIT_EDGES == 2
	int nn = n;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			double l = VEdgeLength[i][j];
			if(l == INFTY)
			{
				continue;
			}

			int ll = (int) l;
			assert(ll);
			nn += ll - 1;
		}
	}

	Assert(nn <= MAXN, "MAXN", nn);

	nn = n;
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			double l = VEdgeLength[i][j];
			if(l == INFTY)
			{
				continue;
			}

			int ms = std::min(MemorySize[i], MemorySize[j]);
			int ll = (int) l;
			for(int k = 0; k < ll; k++)
			{
				int from = (k) ? (nn - 1) : i;
				int to = (k == ll - 1) ? j : (MemorySize[nn] = ms, nn++);
				VIsEdge[from][to] = 1;
			}
		}
	}

	n = nn;
#endif
}


void InitVSucc(bool Check)
{
	for(int i = 0; i < n; i++)
	{
		VInDegree[i] = 0;
		VOutDegree[i] = 0;
	}

#if UNIT_EDGES
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if(VIsEdge[i][j])
			{
				VPred[j][VInDegree[j]++] = (ADJ) {i};
				VSucc[i][VOutDegree[i]++] = (ADJ) {j};
			}
		}
	}
#else
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			double l = VEdgeLength[i][j];
			if(l < INFTY)
			{
				VPred[j][VInDegree[j]++] = (ADJ) {i, l};
				VSucc[i][VOutDegree[i]++] = (ADJ) {j, l};
			}
		}
	}
#endif
		
	if(Check)
	{
		for(int i = 0; i < n; i++)
		{
			assert(VOutDegree[i]);
		}
	}
}


void SetMemorySizes()
{
	switch(type)
	{
		case 1:
		//at random
		{
			for(int i = 0; i < n; i++)
			{
				MemorySize[i] = 1;
			}
			
			for(q = n; q < qTotal; q++)
			{
				MemorySize[rand() % n]++;
			}
			
			break;
		}
		
		case 2:
		//by degrees
		{
			for(int i = 0; i < n; i++)
			{
				MemorySize[i] = 1;
			}
			
			for(q = n; q < qTotal; q++)
			{
				double max = 0, temp;
				int maxi = 0;
				
				for(int i = 0; i < n; i++)
				{
					if((temp = (VInDegree[i] + 0.) / MemorySize[i]) > max)
					{
						max = temp;
						maxi = i;
					}
				}
				
				MemorySize[maxi]++;
			}
			
			break;
		}

		case 5:
		//to all
		{
			for(int i = 0; i < n; i++)
			{
				MemorySize[i] = qTotal / n;
			}
		}
	}
}


void InitFM()
//initializes the arrays Q2V, V2Q, OutDegree, Succ
{
	q = 0;
	for(int i = 0; i < n; i++)
	{
		V2Q[i] = q;
		for(int j = 0; j < MemorySize[i]; j++)
		{
			Q2V[q++] = i;
		}
	}

//here you can hard-code the automaton part (or any restrictions to it)
	for(int i = 0; i < q; i++)
	{
		int v1, e1;
		Unfold(i, v1, e1);
		
		for(int j = 0; j < q; j++)
		{
			int v2, e2;
			Unfold(j, v2, e2);
			
			{
                        #if UNIT_EDGES
				if(VIsEdge[v1][v2])
				{
					GlobSucc[i][GlobOutDegree[i]++] = (ADJ) {j};
				}
                        #else
				if(VEdgeLength[v1][v2] < INFTY)
				{
					GlobSucc[i][GlobOutDegree[i]++] = (ADJ) {j, VEdgeLength[v1][v2]};
				}
                        #endif
			}

		}
	}
}


void InitStrategySucc(STRATEGY& S)
{
	for(int i = 0; i < q; i++)
	{
		S.OutDegree[i] = GlobOutDegree[i];

		for(int j = 0; j < GlobOutDegree[i]; j++)
		{
			S.Succ[i][j] = GlobSucc[i][j];
		}
	}
}


void InitStartingPoint(STRATEGY& S)
//selects a random starting point for the passed strategy
{
	for(int i = 0; i < q; i++)
	{
		for(int j = 0; j < q; j++)
		{
			S.Prob[i][j] = 0.;
		}
	}

	for(int i = 0; i < q; i++)
	{
		int sum = 0;
		for(int j = 0; j < S.OutDegree[i]; j++)
		{
			int k = S.Succ[i][j].To;
			int r = (rand() % 1000) + 1;
			S.Prob[i][k] = r;
			sum += r;
		}
		
		for(int j = 0; j < S.OutDegree[i]; j++)
		{
			int k = S.Succ[i][j].To;
			S.Prob[i][k] /= sum;
		}
	}
}


void FindReachableStates(STRATEGY& S, int* IsReachable)
{
	static int Time = 0;
	static int Stamp[MAXQ];

	int v;
	if(IVal)
	{
		v = q - 1;
	}
	else
	//this should solve the problem of starting
	//in a state which is not reachable again
	{
		Time++;
		v = V2Q[tMaxWeight];
		while(Stamp[v] < Time)
		{
			Stamp[v] = Time;
			v = S.Succ[v][0].To;
		}
	}

	for(int i = 0; i < q; i++)
	{
		IsReachable[i] = 0;
	}
	
	QBeg = 0;
	QEnd = 0;
	Q[QEnd++] = v;
	IsReachable[v] = 1;
	
	while(QBeg < QEnd)
	{
		int cur = Q[QBeg++];
		for(int j = 0; j < S.OutDegree[cur]; j++)
		{
			int s = S.Succ[cur][j].To;
			if(S.Prob[cur][s] <= 0.)
			//this edge is not being used any more
			{
				//printf("RUBBISH!!!\n");
				continue;
			}
			
			if(IsReachable[s])
			//already visited
			{
				continue;
			}
			
			Q[QEnd++] = s;
			IsReachable[s] = 1;
		}
	}

	if(0)
	{
		printf("Reachable states:");
		for(int i = 0; i < q; i++)
		{
			if(IsReachable[i])
			{
				printf(" %s", StateName(i));
			}
		}

		printf("\n");
	}
}


double GetBlindness(int t)
{
#if BLINDNESS
	return Blindness[t];
#else
	return 0.;
#endif
}


void ComputeR(STRATEGY& S)
//computes the values of R and Catch in dependence on the passed strategy
{
#if UNIT_EDGES
	//in the edge-attacks setting, R[t][i][d] is the probability that the defender,
	//provided he is currently at i, gets to a state corresponding to t
	//for the first time (not counting the initial i) in exactly d+1 steps;
	//in the vertex-attacks setting, R[t][i][d] is the probability that the defender,
	//provided he is currently at i, gets to a state corresponding to t
	//for the first time (counting the initial i) in exactly d steps;

	//Catch[t][i] is the probability of covering an attack at vertex t when being at state i,
	//i.e., the sum of R[t][i][d] over d's

	for(int t = 0; t < g; t++)
	{
		for(int i = 0; i < q; i++)
		{
			for(int d = 0; d < Time[t]; d++)
			{
				R[t][i][d] = 0.;
			}
		}
	}

	for(int t = 0; t < g; t++)
	{
		for(int i = 0; i < q; i++)
		{
			R[t][i][0] += (Q2V[i] == t) ? (1. - GetBlindness(t)) : 0.;
		}

		for(int d = 1; d < Time[t]; d++)
		{
			for(int i = 0; i < q; i++)
			{
				double blind1 = 1.;

				if(Q2V[i] == t)
				{
					if((blind1 = GetBlindness(t)) == 0.)
					{
						continue;
					}
				}

				for(int k = 0; k < S.OutDegree[i]; k++)
				{
					int j = S.Succ[i][k].To;

					double blind2 = 1.;
					R[t][i][d] += S.Prob[i][j] * R[t][j][d - 1] * blind1 * blind2;
				}
			}
		}

		for(int i = 0; i < q; i++)
		{
			Catch[t][i] = 0.;
			for(int d = 0; d < Time[t]; d++)
			{
				Catch[t][i] += R[t][i][d];
			}
		}
	}
#else
	static int FirstCall = 1;
	static int InDegree[MAXQ]; //InDegree[i] is the number of predecessors of state i
	static ADJ Pred[MAXQ][MAXQ]; //Pred[i][j] is the j-th predecessor of state i
	static double MaxInLength[MAXQ]; //MaxInLength[i] is the maximum length of an edge
		//going to state i which is used by the current strategy S
	
	for(int i = 0; i < q; i++)
	{
		InDegree[i] = 0;
		MaxInLength[i] = 0.;
	}
	
	for(int i = 0; i < q; i++)
	{
		if(!(IsReachable[i]))
		{
			continue;
		}

		for(int k = 0; k < S.OutDegree[i]; k++)
		{
			int j = S.Succ[i][k].To;
			double l = S.Succ[i][k].Length;
			
			Pred[j][InDegree[j]++] = (ADJ) {i, l};
			Maximize(MaxInLength[j], l);
		}
	}

	if(0)
	for(int i = 0; i < q; i++)
	{
		printf("MaxInLength[%s] = %lf\n", StateName(i), MaxInLength[i]);
	}
	
	long long TotalPV = 0;
	long long TotalPE = 0;
	for(int t = 0; t < g; t++)
	{
		Heap.clear();
		HeapList[t].clear();
		
		LabelO2N[t].clear();
		nLabels[t] = 0;
		Contrib[t].clear();
		
		for(int i = 0; i < q; i++)
		{
			Catch[t][i] = 0.;

			if(Q2V[i] == t)
			{
				LabelO2N[t].push_back(nLabels[t]);
				Heap.push_back((HEAPITEM) {0., 1. - GetBlindness(t), i, nLabels[t]++});
			}
		}

		static std::pair<int, double> CurListIndex[MAXQ];
		for(int i = 0; i < q; i++)
		{
			CurListIndex[i] = std::make_pair(-1, -INFTY);
		}
		
		while(!(Heap.empty()))
		{
			int iList = HeapList[t].size();

			while(1)
			{
				HEAPITEM cur = Heap[0];
				std::pop_heap(Heap.begin(), Heap.end());
				Heap.pop_back();

				if(cur.d < CurListIndex[cur.i].second + PATH_MERGE_THRESHOLD)
				{
					if(0) printf("Meeting {%d, %lf, %lf, %d}, merging with heap item %d\n",
					       cur.i, cur.d, cur.p, cur.id, CurListIndex[cur.i].first);
					CurListIndex[cur.i].second = cur.d;
					LabelO2N[t][cur.id] = CurListIndex[cur.i].first;
					HeapList[t][CurListIndex[cur.i].first].d = cur.d;
					HeapList[t][CurListIndex[cur.i].first].p += cur.p;
				}
				else
				{
					if(0) printf("Meeting {%d, %lf, %lf, %d}, setting new heap item %d\n",
					       cur.i, cur.d, cur.p, cur.id, (int) HeapList[t].size());
					CurListIndex[cur.i] = std::make_pair(HeapList[t].size(), cur.d);
					LabelO2N[t][cur.id] = HeapList[t].size();
					HeapList[t].push_back(cur);
				}

				if((Heap.empty()) || (Heap[0].d >= cur.d + PATH_MERGE_THRESHOLD))
				{
					break;
				}
			}

			if(0) printf("Running [%d..%d)\n", iList, (int) HeapList[t].size());
			
			for(; iList < (int) HeapList[t].size(); iList++)
			{
				HEAPITEM cur = HeapList[t][iList];
				int i = cur.i;
				Catch[t][i] += cur.p;
				if(0) printf("Currently added %lf for state %s -> target %c\n",
				  cur.p, StateName(i), VertexName(t));

				for(int k = 0; k < InDegree[i]; k++)
				{
					int j = Pred[i][k].To;

					double blind = 1.;
					if(Q2V[j] == t)
					{
						if((blind = GetBlindness(t)) == 0.)
						{
							continue;
						}
					}

					double d = cur.d + Pred[i][k].Length;

					if(d + MaxInLength[j] <= Time[t])
					{
						Contrib[t].push_back(std::make_pair(cur.id, nLabels[t]));
						LabelO2N[t].push_back(nLabels[t]);
						Heap.push_back((HEAPITEM) {d, cur.p * S.Prob[j][i] * blind, j, nLabels[t]++});
						std::push_heap(Heap.begin(), Heap.end());
					}
				}
			}
		}

		if((1) && (FirstCall)) printf("%d contribs and %d final heap items for target %c\n",
		  (int) Contrib[t].size(), (int) HeapList[t].size(), VertexName(t));
		TotalPE += Contrib[t].size();
		TotalPV += HeapList[t].size();
	}

	if((1) && (FirstCall)) printf("PE = %lld, PV = %lld\n", TotalPE, TotalPV);
	FirstCall = 0;

	if(0)
	{
		exit(0);
	}
#endif

	for(int t = 0; t < g; t++)
	{
		for(int i = 0; i < q; i++)
		{
			Loss[t][i] = (Catch[t][i] - 1.) * Weight[t];
			if(0) printf("Catching attack at %c when at %s: prob %lf, loss %lf\n",
			  VertexName(t), StateName(i), Catch[t][i], Loss[t][i]);
		}
	}
}


void ComputePD(int t, int a, int b)
//computes the values of PD and TotalPD for a given vertex t and edge (a,b)
//in dependence on the current strategy
{
	nPDCalls++;

#if UNIT_EDGES
	for(int i = 0; i < q; i++)
	{
		for(int d = 0; d < Time[t]; d++)
		{
			PD[i][d] = 0.;
		}
	}

	for(int d = 1; d < Time[t]; d++)
	{
		for(int i = 0; i < q; i++)
		{
			double blind1 = 1.;
			if(Q2V[i] == t)
			{
				if((blind1 = GetBlindness(t)) == 0.)
				{
					continue;
				}
			}

			for(int k = 0; k < S.OutDegree[i]; k++)
			{
				int j = S.Succ[i][k].To;

				double blind2 = 1.;

				PD[i][d] += S.Prob[i][j] * PD[j][d - 1] * blind1 * blind2;

				if(a == i)
				//this edge goes from the same vertex as the one with respect to which
				//we are computing the current partial derivative
				{
					if(b == j)
					//this is exactly the edge with respect to which
					//we are computing the current partial derivative
					{
						PD[i][d] += R[t][j][d - 1] * blind1 * blind2;
					}

                                #if REPLACEMENT_EDGES
					if(!(k))
					//this is a replacement edge
					{
						PD[i][d] -= R[t][j][d - 1] * blind1 * blind2;
					}
                                #else
					PD[i][d] -= S.Prob[i][j] * R[t][j][d - 1] * blind1 * blind2;
                                #endif
				}
			}
		}
	}

	for(int i = 0; i < q; i++)
	{
		TotalPD[i] = 0.;
		for(int d = 0; d < Time[t]; d++)
		{
			TotalPD[i] += PD[i][d];
		}
	}
#else
	for(int i = 0; i < q; i++)
	{
		TotalPD[i] = 0.;
	}

	PD.clear();
	for(int i = 0; i < (int) HeapList[t].size(); i++)
	{
		PD.push_back(0.);
	}
	
	for(auto& contrib: Contrib[t])
	{
		int r = LabelO2N[t][contrib.first];
		int s = LabelO2N[t][contrib.second];
		
		int j = HeapList[t][r].i; //search source, edge dest
		int i = HeapList[t][s].i; //search dest, edge source
		
		double diff = 0.;
		
		//PD[i][d] += S.Prob[i][j] * PD[j][d - 1];
		diff += S.Prob[i][j] * PD[r];

		if(a == i)
		//this edge goes from the same vertex as the one with respect to which
		//we are computing the current partial derivative
		{
			if(b == j)
			//this is exactly the edge with respect to which
			//we are computing the current partial derivative
			{
				//PD[i][d] += R[t][j][d - 1];
				diff += HeapList[t][r].p;
			}

                #if REPLACEMENT_EDGES
			if(S.Succ[i][0].To == j)
			//this is a replacement edge
			{
				//PD[i][d] -= R[t][j][d - 1];
				diff -= HeapList[t][r].p;
			}
                #else
			//PD[i][d] -= S.Prob[i][j] * R[t][j][d - 1];
			diff -= S.Prob[i][j] * HeapList[t][r].p;
                #endif
		}

		if(Q2V[i] == t)
		{
			diff *= GetBlindness(t);
		}

		if(0) printf("Contribution %lf to %d from {%d, %lf, %lf, %d} to {%d, %lf, %lf, %d}\n",
		       diff, i,
		       HeapList[t][r].i, HeapList[t][r].d, HeapList[t][r].p, HeapList[t][r].id,
		       HeapList[t][s].i, HeapList[t][s].d, HeapList[t][s].p, HeapList[t][s].id);
		
		PD[s] += diff;
		TotalPD[i] += diff;
	}
#endif
}


void Minimize(double& a, double b)
//assigns b to a provided the new value is less
{
	if(b < a)
	{
		a = b;
	}
}


void Maximize(double& a, double b)
//assigns b to a provided the new value is greater
{
	if(b > a)
	{
		a = b;
	}
}


double EvaluateStrategy(STRATEGY& S)
//returns (a lower bound on) the value of the current strategy
{
	FindReachableStates(S, IsReachable);
	ComputeR(S);

	S.Value = 0.; //it cannot be greater
	for(int t = 0; t < g; t++)
	{
		MinLoss[t] = 0.; //it cannot be greater
	}

	for(int t = 0; t < g; t++)
	//the attacker may attack at any target
	{
		for(int i = 0; i < q; i++)
		//the defender may be at any state
		{
			if(!(IsReachable[i]))
			//unreachable states are not considered
			{
				if(0) printf("Attack at %c from %s is skipped\n", VertexName(t), StateName(i));
				continue;
			}
			
			if(0) printf("Attack at %c from %s has loss %lf\n", VertexName(t), StateName(i), Loss[t][i]);
			Minimize(MinLoss[t], Loss[t][i]);
		}
		
		if(0) printf("Attack at %c has loss %lf\n", VertexName(t), MinLoss[t]);
		Minimize(S.Value, MinLoss[t]);
	}

	return S.Value;
}


void Normalize(STRATEGY& S, int i)
//computes the value of the replacement variable from state i
{
#if REPLACEMENT_EDGES
	double sum = 0.;
	for(int j = 1; j < S.OutDegree[i]; j++)
	//the replacement is for j = 0
	{
		int k = S.Succ[i][j].To;
		Minimize(S.Prob[i][k], 1.);
		Maximize(S.Prob[i][k], 0.);
		sum += S.Prob[i][k];
	}

	if(sum > 1.)
	//must not leave it negative
	{
		for(int j = 1; j < S.OutDegree[i]; j++)
		//the replacement is for j = 0
		{
			int k = S.Succ[i][j].To;
			S.Prob[i][k] /= sum;
		}

		sum = 1.;
	}

	int k = S.Succ[i][0].To;
	S.Prob[i][k] = 1. - sum;
#else
	double sum = 0.;
	for(int j = 0; j < S.OutDegree[i]; j++)
	{
		int k = S.Succ[i][j].To;
		Minimize(S.Prob[i][k], 1.);
		Maximize(S.Prob[i][k], 0.);
		sum += S.Prob[i][k];
	}

	for(int j = 0; j < S.OutDegree[i]; j++)
	{
		int k = S.Succ[i][j].To;
		S.Prob[i][k] /= sum;
	}
#endif
}


void NormalizeAll(STRATEGY& S)
//calls to compute the values of all the replacement variables
{
	for(int i = 0; i < q; i++)
	{
		Normalize(S, i);
	}
}


void DisableEdge(STRATEGY& S, int i, int k)
//disables the k-th successor of vertex i
{
	int j = S.Succ[i][k].To;
	S.Prob[i][j] = 0.;
	S.Succ[i][k] = S.Succ[i][--S.OutDegree[i]];

	if(0)
	{
		printf("Edge %s->%s just disabled\n", StateName(i), StateName(j, AuxBuffer));
	}
}


bool DisableLeastUsedEdge()
//finds the least used edge and calls to disable it
{
	double min = 2.; //it cannot be this high
	int rmin = 0, jmin = 0;
	for(int r = 0; r < q; r++)
	{
		if(S.OutDegree[r] == 1)
		//cannot decrease the out-degree to 0
		{
			continue;
		}

		for(int j = 0; j < S.OutDegree[r]; j++)
		{
			int s = S.Succ[r][j].To;
			if(S.Prob[r][s] < min)
			{
				min = S.Prob[r][s];
				rmin = r;
				jmin = j;
			}
		}
	}

	if(min == 2.)
	//no edge to be deleted
	{
		return 0;
	}

	DisableEdge(S, rmin, jmin);
	Normalize(S, rmin);
	return 1;
}


bool DisableLongEdge()
//in long-edges setting, finds the most problematic edge and disables it
{
#if UNIT_EDGES
	return 0;
#else
	int mint = -1;
	double minloss = 1.; //it cannot be this high
	for(int t = 0; t < g; t++)
	//looking for the most problematic target
	{
		if(MinLoss[t] < minloss)
		{
			minloss = MinLoss[t];
			mint = t;
		}
	}

	if(0) printf("mint is %c, loss %lf\n", VertexName(mint), minloss);
	assert(mint >= 0);

	int mini = -1;
	for(int i = 0; i < q; i++)
	//looking for the most problematic state as of initiating an attack at mint
	{
		if(!(IsReachable[i]))
		{
			continue;
		}

		if(Loss[mint][i] == minloss)
		{
			mini = i;
			break;
		}
	}

	if(0) printf("maxi is %s, loss %lf\n", StateName(mini), minloss);
	assert(mini >= 0);

	int maxj = -1;
	int maxk = -1;
	double maxl = -1.; //it cannot be this low
	for(int j = 0; j < q; j++)
	{
		for(int k = 0; k < S.OutDegree[j]; k++)
		{
			if((S.Succ[j][k].To == mini) && (S.Succ[j][k].Length > maxl))
			{
				maxl = S.Succ[j][k].Length;
				maxj = j;
				maxk = k;
			}
		}
	}

	if(maxj < 0)
	//this can happen only if we begin in a state which is not reachable again
	{
		return 0;
	}

	if(S.OutDegree[maxj] == 1)
	//cannot decrease the out-degree to 0
	{
		return 0;
	}

	if(0) printf("Disabling long problematic edge %s->%s\n", StateName(maxj), StateName(S.Succ[maxj][maxk].To, AuxBuffer));

	DisableEdge(S, maxj, maxk);
	Normalize(S, maxj);
	return 1;
#endif
}


bool DisableSomeEdge(int trial)
//in long-edges setting, the gradient ascent combines disabling
//least used edges and long problematic edges
{
#if (UNIT_EDGES) || (!(DISABLE_LONG_EDGES))
	return DisableLeastUsedEdge();
#else
	int rem = trial % 3;
	if(!(rem))
	//trial = 1,4,7,... cut least use edges;
	//trial = 2,5,8,... cut long problematic edges;
	//trial = 3,6,9,... always choose at random between those two
	{
		rem += (rand() % 2) + 1;
	}

	return (rem == 1) ? DisableLeastUsedEdge() : DisableLongEdge();
#endif
}


void Ascend(int trial)
//this function makes the iterations of the gradient ascent
//until it finds out that no more iterations are desirable
{
	PrevS.Value = -(MaxWeight + 1.); //it cannot be this low
	int nReturns = 0;
	double dc = INITIAL_DC; //the distance coefficient
	nIterations = 0;

	while(1)
	//an iteration of the gradient ascent
	{
		dc *= DC_MULT_FACTOR; //decreasing the distance coefficient a trifle
		EvaluateStrategy(S);

#if REPORT_ITERATIONS
		printf("Current strategy:\n");
		PrintStrategy(S);
		getchar();
#endif

		if(S.Value > BestS.Value)
		{
			memcpy(&BestS, &S, sizeof(STRATEGY));
		}

		if(S.Value >= 0.)
		{
			return;
		}

		if(S.Value <= -MaxWeight)
		{
                #if DISABLE_LONG_EDGES
			if(!(DisableLongEdge()))
                #endif
			{
				return;
			}

			continue;
		}

		if(S.Value <= PrevS.Value)
		//we moved too far - the current strategy is not better than the previous one
		{
			dc *= DC_RETURN_MULT_FACTOR; //decreasing the distance coefficient a bit more

			for(int r = 0; r < q; r++)
			{
				for(int j = 0; j < S.OutDegree[r]; j++)
				{
					int s = S.Succ[r][j].To;
					S.Prob[r][s] = (S.Prob[r][s] + PrevS.Prob[r][s]) * .5;
				}
			}

			nReturns++;
			if(nReturns == N_RETURNS_THRESHOLD)
			{
				PrevS.Value = -(MaxWeight + 1.); //it cannot be this low
				dc = INITIAL_DC;
				nReturns = 0;

				if(!(DisableSomeEdge(trial)))
				{
					return;
				}
			}

			continue;
		}
		else if(S.Value < PrevS.Value + ITERATION_THRESHOLD)
		//the current improvement is low - we are probably very close to a local maximum
		{
			if(!(DisableSomeEdge(trial)))
			{
				return;
			}

			continue;
		}

		nIterations++;
		nReturns = 0;
		memcpy(&PrevS, &S, sizeof(STRATEGY));
		
		for(int a = 0; a < q; a++)
		{
			for(int b = 0; b < q; b++)
			{
				Diff[a][b] = 0.;
			}
		}

		for(int t = 0; t < g; t++)
		{
			const bool VERBOSE = 0;
			if(MinLoss[t] - S.Value > IMPORTANCE_THRESHOLD)
			//this vertex is always well-covered/valueless
			{
				if(VERBOSE) printf("Ignoring attack at %c\n", VertexName(t));
				continue;
			}

			if(VERBOSE) printf("Differentiating attack at %c...\n", VertexName(t));
			
			for(int i = 0; i < q; i++)
			{
				if(!(IsReachable[i]))
				{
					Imp[i] = 0.;
					continue;
				}

				double d = Loss[t][i] - S.Value;
				Imp[i] = (IMPORTANCE_THRESHOLD - d) / IMPORTANCE_THRESHOLD;

				if(Imp[i] > 0.)
				{
					if(VERBOSE) printf("Attack at %c when at %s has importance %lf\n",
					  VertexName(t), StateName(i), Imp[i]);
				}
			}
			
			for(int a = 0; a < q; a++)
			{
                        #if REPLACEMENT_EDGES
				for(int c = 1; c < S.OutDegree[a]; c++)
                        #else
				for(int c = 0; c < S.OutDegree[a]; c++)
                        #endif
				//the replacement is for c = 0
				{
					int b = S.Succ[a][c].To;
					ComputePD(t, a, b);
					
					for(int i = 0; i < q; i++)
					{
						if(Imp[i] > 0.)
						{
							if(VERBOSE)
							{
								printf("Attack at %c when at %s: ", VertexName(t), StateName(i));
								printf("edge %s->%s has contribution %lf\n",
								     StateName(a), StateName(b, AuxBuffer), TotalPD[i]);
							}

							Diff[a][b] += TotalPD[i] * Imp[i] * dc;
						}
					}
				}
			}
		}
		
		for(int a = 0; a < q; a++)
		{
			for(int b = 0; b < q; b++)
			{
				S.Prob[a][b] += Diff[a][b];
			}
		}
		
		NormalizeAll(S);

		for(int r = 0; r < q; r++)
		{
		AGAIN:
			for(int j = 0; j < S.OutDegree[r]; j++)
			{
				int s = S.Succ[r][j].To;
				double thres = (DIVIDE_EDGE_DISABLING_THRESHOLD)
				  ? (EDGE_DISABLING_THRESHOLD / S.OutDegree[r])
				  : EDGE_DISABLING_THRESHOLD;

				if(S.Prob[r][s] <= thres)
				{
					PrevS.Value = -(MaxWeight + 1.); //it cannot be this low
					dc = INITIAL_DC;

					DisableEdge(S, r, j);
					Normalize(S, r);
					goto AGAIN;
				}
			}
		}
	}
}


void ComputeSwitchingR()
{
	static STRATEGY S1;
	fprintf(stderr, "Loading first strategy... ");
	fflush(stderr);
	LoadStrategy(S1);
	fprintf(stderr, "OK\n");
	EvaluateStrategy(S1);
	PrintStrategy(S1);

	fprintf(stderr, "Loading second strategy... ");
	fflush(stderr);
	LoadStrategy(S);
	fprintf(stderr, "OK\n");
	EvaluateStrategy(S);
	PrintStrategy(S);
	
	int IsReachable1[MAXQ];
	FindReachableStates(S1, IsReachable1);

	double SwitchLoss = 0.;

#if UNIT_EDGES
	static double R1[MAXQ][MAXQ][MAXD];
	//R1[i][j][d] is the probability of traveling path i->j in exactly d steps
	//without visiting a state corresponding to the currently examined target t;
	//analogously (d+1, disregarding the initial i) for the edge-attacks setting

	static double ReachWithin2[MAXQ][MAXD];
	//ReachWithin2[i][d] is the probability of catching an attack at the curently examined target t
	//within d steps when being at i;
	//analogously (d+1, disregarding the initial i) for the edge-attacks setting

	for(int t = 0; t < g; t++)
	{
	//computing the second part (after the change)
		for(int i = 0; i < q; i++)
		{
			for(int d = 0; d < Time[t]; d++)
			{
				R[t][i][d] = 0.;
			}
		}

		for(int i = 0; i < q; i++)
		{
			R[t][i][0] = (Q2V[i] == t) ? 1. : 0.;
		}

		for(int d = 1; d < Time[t]; d++)
		{
			for(int i = 0; i < q; i++)
			{
				if(Q2V[i] == t)
				{
					R[t][i][d] = 0.;
					continue;
				}

				for(int j = 0; j < S.OutDegree[i]; j++)
				{
					int k = S.Succ[i][j].To;

					if((Q2V[k] != t) || (d == 1))
					{
						R[t][i][d] += S.Prob[i][k] * R[t][k][d - 1];
					}
				}
			}
		}

		for(int i = 0; i < q; i++)
		{
			ReachWithin2[i][0] = R[t][i][0];
			for(int d = 1; d < Time[t]; d++)
			{
				ReachWithin2[i][d] = ReachWithin2[i][d - 1] + R[t][i][d];
			}
		}

	//computing the first part (before the change)
		for(int i = 0; i < q; i++)
		{
			for(int j = 0; j < q; j++)
			{
				for(int d = 0; d < Time[t]; d++)
				{
					R1[i][j][d] = 0.;
				}
			}
		}

		for(int i = 0; i < q; i++)
		{
			R1[i][i][0] = (Q2V[i] == t) ? 0. : 1.;
		}

		for(int d = 1; d < Time[t]; d++)
		{
			for(int i = 0; i < q; i++)
			{
				if(!(IsReachable1[i]))
				{
					continue;
				}
				
				for(int j = 0; j < q; j++)
				{
					if(Q2V[j] == t)
					{
						continue;
					}

					for(int a = 0; a < S1.OutDegree[j]; a++)
					{
						int k = S1.Succ[j][a].To;
						R1[i][k][d] += R1[i][j][d - 1] * S1.Prob[j][k];
					}
				}
			}
		}

	//evaluating the loss
		for(int d = 1; d < Time[t]; d++)
		{
			printf("Environment change in %d more step(s)...\n", d + 0);
			for(int i = 0; i < q; i++)
			//initial state
			{
				if(!(IsReachable1[i]))
				{
					continue;
				}
				
				double Miss = 0.;
				for(int j = 0; j < q; j++)
				//intermediate state
				{
					Miss += R1[i][j][d] * (1. - ReachWithin2[i][Time[t] - (d + 1)]);
					if(0) printf("Miss increased by %lf * %lf\n", R1[i][j][d], 1. - ReachWithin2[i][(Time[t] - 1) - d]);
				}

				double prob = 1. - Miss;
				double loss = (prob - 1.) * Weight[t];
				Minimize(SwitchLoss, loss);

				printf("Catching attack at %c when at %s: prob %lf, loss %lf\n",
				  VertexName(t), StateName(i), prob, loss);
			}
		}
	}
#else
	//Now the difficult part starts...
	static int InDegree[MAXQ]; //InDegree[i] is the number of predecessors of state i
	static ADJ Pred[MAXQ][MAXQ]; //Pred[i][j] is the j-th predecessor of state i
	static double MaxInLength[MAXQ]; //MaxInLength[i] is the maximum length of an edge
	        //going to state i which is used by the current strategy S

	for(int i = 0; i < q; i++)
	{
		InDegree[i] = 0;
		MaxInLength[i] = 0.;
	}

	for(int i = 0; i < q; i++)
	{
		for(int k = 0; k < S.OutDegree[i]; k++)
		{
			int j = S.Succ[i][k].To;
			double l = S.Succ[i][k].Length;

			Pred[j][InDegree[j]++] = (ADJ) {i, l};
		}
	}

	for(int i = 0; i < q; i++)
	{
		if(!(IsReachable1[i]))
		{
			continue;
		}

		for(int k = 0; k < S1.OutDegree[i]; k++)
		{
			int j = S1.Succ[i][k].To;
			double l = S1.Succ[i][k].Length;

			Maximize(MaxInLength[j], l);
		}
	}

	if(0)
	for(int i = 0; i < q; i++)
	{
		printf("MaxInLength[%s] = %lf\n", StateName(i), MaxInLength[i]);
	}

	printf("Evaluating switching value...\n");
	for(int t = 0; t < g; t++)
	{
		printf("Attack at target %c...\n", VertexName(t));
	//computing the second part (after the change)
		Heap.clear();
		HeapList[t].clear();

		LabelO2N[t].clear();
		nLabels[t] = 0;
		Contrib[t].clear();

		for(int i = 0; i < q; i++)
		{
			Catch[t][i] = 0.;

			if(Q2V[i] == t)
			{
				LabelO2N[t].push_back(nLabels[t]);
				Heap.push_back((HEAPITEM) {0., 1., i, nLabels[t]++});
			}
		}

		static std::pair<int, double> CurListIndex[MAXQ];
		for(int i = 0; i < q; i++)
		{
			CurListIndex[i] = std::make_pair(-1, -INFTY);
		}

		while(!(Heap.empty()))
		{
			int iList = HeapList[t].size();

			while(1)
			{
				HEAPITEM cur = Heap[0];
				std::pop_heap(Heap.begin(), Heap.end());
				Heap.pop_back();

				if(cur.d < CurListIndex[cur.i].second + PATH_MERGE_THRESHOLD)
				{
					if(0) printf("Meeting {%d, %lf, %lf, %d}, merging with heap item %d\n",
					       cur.i, cur.d, cur.p, cur.id, CurListIndex[cur.i].first);
					CurListIndex[cur.i].second = cur.d;
					LabelO2N[t][cur.id] = CurListIndex[cur.i].first;
					HeapList[t][CurListIndex[cur.i].first].d = cur.d;
					HeapList[t][CurListIndex[cur.i].first].p += cur.p;
				}
				else
				{
					if(0) printf("Meeting {%d, %lf, %lf, %d}, setting new heap item %d\n",
					       cur.i, cur.d, cur.p, cur.id, (int) HeapList[t].size());
					CurListIndex[cur.i] = std::make_pair(HeapList[t].size(), cur.d);
					LabelO2N[t][cur.id] = HeapList[t].size();
					HeapList[t].push_back(cur);
				}

				if((Heap.empty()) || (Heap[0].d >= cur.d + PATH_MERGE_THRESHOLD))
				{
					break;
				}
			}

			if(0) printf("Running [%d..%d)\n", iList, (int) HeapList[t].size());

			for(; iList < (int) HeapList[t].size(); iList++)
			{
				HEAPITEM cur = HeapList[t][iList];
				int i = cur.i;
				Catch[t][i] += cur.p; //it needs better information
				if(0) printf("Currently added %lf for state %s -> target %c\n",
				  cur.p, StateName(i), VertexName(t));

				for(int k = 0; k < InDegree[i]; k++)
				{
					int j = Pred[i][k].To;
					if(Q2V[j] == t)
					{
						continue;
					}

					double d = cur.d + Pred[i][k].Length;

					if(d <= Time[t])
					{
						Contrib[t].push_back(std::make_pair(cur.id, nLabels[t]));
						LabelO2N[t].push_back(nLabels[t]);
						Heap.push_back((HEAPITEM) {d, cur.p * S.Prob[j][i], j, nLabels[t]++});
						std::push_heap(Heap.begin(), Heap.end());
					}
				}
			}
		}

		if(0) printf("%d contribs and %d final heap items for target %c\n",
		  (int) Contrib[t].size(), (int) HeapList[t].size(), VertexName(t));
		  
	//computing the first part (before the change)
	    for(int i = 0; i < q; i++)
	    {
		        if(!(IsReachable1[i]))
			{
				continue;
			}

			if(Q2V[i] == t)
			{
				continue;
			}

			printf("  Going to %s (MaxInLength is %lf)...\n", StateName(i), MaxInLength[i]);
			
			std::vector<HEAPITEM> Heap1;
			std::vector<HEAPITEM> HeapList1;

			Heap1.push_back((HEAPITEM) {MaxInLength[i], 1., i, -1});

			for(int j = 0; j < q; j++)
			{
				CurListIndex[j] = std::make_pair(-1, -INFTY);
			}

			while(!(Heap1.empty()))
			{
				int iList = HeapList1.size();

				while(1)
				{
					HEAPITEM cur = Heap1[0];
					std::pop_heap(Heap1.begin(), Heap1.end());
					Heap1.pop_back();

					if(cur.d < CurListIndex[cur.i].second + PATH_MERGE_THRESHOLD)
					{
						if(0) printf("Meeting {%d, %lf, %lf, %d}, merging with heap item %d\n",
						       cur.i, cur.d, cur.p, cur.id, CurListIndex[cur.i].first);
						CurListIndex[cur.i].second = cur.d;
						HeapList1[CurListIndex[cur.i].first].d = cur.d;
						HeapList1[CurListIndex[cur.i].first].p += cur.p;
					}
					else
					{
						if(0) printf("Meeting {%d, %lf, %lf, %d}, setting new heap item %d\n",
						       cur.i, cur.d, cur.p, cur.id, (int) HeapList1.size());
						CurListIndex[cur.i] = std::make_pair(HeapList1.size(), cur.d);
						HeapList1.push_back(cur);
					}

					if((Heap1.empty()) || (Heap1[0].d >= cur.d + PATH_MERGE_THRESHOLD))
					{
						break;
					}
				}

			//evaluating the loss
				printf("    Environment change in %lf time units... ", HeapList1.back().d);
				double catchprob = 0.;
				double primarycatchprob = 1.;
				for(int j = iList; j < (int) HeapList1.size(); j++)
				{
					HEAPITEM cur = HeapList1[j];
					primarycatchprob -= cur.p;

					for(int k = 0; k < (int) HeapList[t].size(); k++)
					{
						HEAPITEM cur2 = HeapList[t][k];
						if((cur2.i == cur.i) && (cur.d + cur2.d <= Time[t]))
						{
							catchprob += cur.p * cur2.p;
						}
					}
				}

				for(int j = 0; j < (int) Heap1.size(); j++)
				{
					HEAPITEM cur = Heap1[j];
					primarycatchprob -= cur.p;

					for(int k = 0; k < (int) HeapList[t].size(); k++)
					{
						HEAPITEM cur2 = HeapList[t][k];
						if((cur2.i == cur.i) && (cur.d + cur2.d <= Time[t]))
						{
							catchprob += cur.p * cur2.p;
						}
					}
				}

				double prob = primarycatchprob + catchprob;
				double loss = (prob - 1.) * Weight[t];
				Minimize(SwitchLoss, loss);
				printf("prob %lf, loss %lf\n", prob, loss);

				if(0) printf("Running [%d..%d)\n", iList, (int) HeapList1.size());

				for(; iList < (int) HeapList1.size(); iList++)
				{
					HEAPITEM cur = HeapList1[iList];
					int a = cur.i;

					for(int k = 0; k < S1.OutDegree[a]; k++)
					{
						int b = S1.Succ[a][k].To;
						if(Q2V[b] == t)
						{
							continue;
						}

						double d = cur.d + S1.Succ[a][k].Length;

						if(d <= Time[t])
						{
							Heap1.push_back((HEAPITEM) {d, cur.p * S1.Prob[a][b], b, -1});
							std::push_heap(Heap1.begin(), Heap1.end());
						}
					}
				}
			}
		}
	}
#endif

	printf("Switching ");
	PrintValue(SwitchLoss);
}


int main(int argc, char** argv)
{
	int seed = SEED, nTrials = N_TRIALS;
	if(argc >= 2)
	//the seed is provided as a command-line argument
	{
		sscanf(argv[1], "%d", &seed);
	}

	if(argc >= 3)
	//the number of trials is provided as a command-line argument
	{
		sscanf(argv[2], "%d", &nTrials);
	}

	srand((seed) ? seed : time(NULL));
	//seed = 0 corresponds to random seed

	LoadGraph();
	LoadType();

	for(int t = 0; t < g; t++)
	{
		if(Weight[t] > MaxWeight)
		{
			MaxWeight = Weight[t];
			tMaxWeight = t;
		}
	}

	assert(MaxWeight > 0.);

#if UNIT_EDGES != 2
	InitVSucc(1);
#endif
	SetMemorySizes();

#if UNIT_EDGES != 1
	static double VDist[MAXN][MAXN];
	FloydWarshall(VDist, 1e-6);

	for(int i = 0; i < n; i++)
	{
		if(VDist[i][i] < 0.)
		//a cycle of zero length
		{
			fprintf(stderr, "Vertex %d lies on a cycle of zero length!\n", i);
			exit(0);
		}
	}

	if((seed >= 0) && (VISITING_BONUS > 0.))
	//deleting useless long edges (not if loading a strategy
	//which was computed for a possibly different environment)
	{
        #if (REPORT_INPUT) && (UNIT_EDGES != 2)
		PrintInput();
	#endif

		FloydWarshall(VDist, VISITING_BONUS);

		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++)
			{
				if((i != j) && (VDist[i][j] < VEdgeLength[i][j] - VISITING_BONUS))
				{
				#if REPORT_INPUT
					printf("Deleting edge %d->%d because its length %lf is worse than a path of length %lf\n",
					       VertexName(i), VertexName(j), VEdgeLength[i][j] - VISITING_BONUS, VDist[i][j]);
				#endif
					VEdgeLength[i][j] = INFTY;
				}
			}
		}

        #if UNIT_EDGES != 2
		InitVSucc(1);
		SetMemorySizes();
        #endif
	}
#endif

#if UNIT_EDGES == 2
	ToUnitEdges();
	InitVSucc(1);
#endif

#if REPORT_INPUT
	PrintInput();
	getchar();

	if(IVal)
	{
		printf("Computing IVal...\n");
	}
#endif

#if RELATIVE_THRESHOLDS
	for(int t = 0; t < g; t++)
	//the targets' values are normalized
	//so that the maximum value is 1.
	{
		Weight[t] /= MaxWeight;
	}

	OrigMaxWeight = MaxWeight;
	MaxWeight = 1.;
#endif
	

	InitFM();
	AbsBestS.Value = -(MaxWeight + 1.); //it cannot be this low

	if(seed == -2)
	{
		fprintf(stderr, "Computing switching coverage...\n");
		ComputeSwitchingR();
		return 0;
	}

	for(int trial = 1; (!(nTrials)) || (trial <= nTrials); trial++)
	{
		BestS.Value = -(MaxWeight + 1.); //it cannot be this low

		InitStrategySucc(S);
		InitStartingPoint(S);

		if((seed == -1) && (trial == 1))
		//loading the seed
		{
			LoadStrategy(S);

			if(1)
			{
				EvaluateStrategy(S);

				if(0)
				{
					printf("Loaded strategy:\n");
					PrintStrategy(S);
				}

				printf("Init");
				PrintValue(S.Value);

				if(0) return 0;
			}
		}

		Ascend(trial);

#if REPORT_TRIALS == 2
		printf("Trial #%d: ", trial);
		PrintValue(BestS.Value);

		if(1)
		{
			printf("Trial #%d: nIterations: %d\n\n", trial, nIterations);
		}
#elif REPORT_TRIALS == 3
		printf("Trial #%d:\n", trial);
		PrintStrategy(BestS);
#endif

		if(BestS.Value > AbsBestS.Value)
		{
			memcpy(&AbsBestS, &BestS, sizeof(STRATEGY));

		#if REPORT_TRIALS
			printf("New strategy (temporarily best):\n");
			PrintStrategy(AbsBestS);
		#endif
		}
		else
		{
                #if TRIAL_REPORT_PERIOD
			if(!(trial % TRIAL_REPORT_PERIOD))
			{
				printf("Best of %d trials:\n", trial);
				PrintStrategy(AbsBestS);
			}
                #endif
		}
	}

	printf("Best of all %d trials:\n", nTrials);
	PrintStrategy(AbsBestS);

	printf("Final ");
	PrintValue(AbsBestS.Value);

	if(0)
	{
		printf("%d PD calls\n", nPDCalls);
	}

	return 0;
}
