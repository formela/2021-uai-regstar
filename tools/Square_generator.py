#!/usr/bin/env python3

from xml.dom import minidom
import random
import math
import sys, getopt, re


#######################
# parameters

# default setting
choose_number = None
choose_list = None
outputfile = None
rseed = 13
g_size = 5

HelpInfo = """Options:
   -g <number>
          grid size

   -s <number>
          seed for random generator

   -n <number>
          number of nodes to be chosen

   -d <space separated list of chosen nodes>
          choose exactly the nodes in the space separated list
          e.g. -d "11 21 33 41 52"

   -o <outputfile>, --ofile <outputfile>
          output file name, (default = Subsquare_[chosen_nodes].kml)"""


try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:g:s:d:o:",["ofile="])
except getopt.GetoptError:
    print(HelpInfo)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print(HelpInfo)
        sys.exit()
    elif opt in ("-g"):
        g_size = int(arg)
    elif opt in ("-s"):
        rseed = int(arg)
    elif opt in ("-n"):
        choose_number = int(arg)
    elif opt in ("-d"):
        choose_list = list(map(int, arg.split()))
    elif opt in ("-o", "--ofile"):
        outputfile = arg

# print('Choose number is ', choose_number)
# print('Choose list is ', choose_list)
# print('Output file is ', outputfile)


# default values
random.seed(rseed)
pool = [(x+1)*10+(y+1) for x in range(g_size) for y in range(g_size)]
random.shuffle(pool)

# sanity check
if not (1 <= g_size <= 9):
    print(f"Error: wrong value for parameter -g, not (1 <= {g_size} <= 9).")
    sys.exit(2)
if choose_number is not None and not (1 <= choose_number <= g_size*g_size):
    print(f"Error: wrong value for parameter -n, not (1 <= {choose_number} <= {g_size*g_size}).")
    sys.exit(2)


if choose_list is not None:
    new_list=[]
    for i in choose_list:
        if i in pool and i not in new_list: new_list.append(i)
    if len(new_list) < len(choose_list):
        print("Warning: choose_list reduced to ", new_list)
        choose_list = new_list

# default values
if choose_number is None and choose_list is None:
    print("Warning: Neither -n nor -d is defined. Full square will be produced.")
    choose_number=g_size*g_size
    choose_list = pool
elif choose_list is None:
    choose_list = pool[:choose_number]
elif choose_number is None:
    choose_number = len(choose_list)
elif choose_number < len(choose_list):
    choose_list = choose_list[:choose_number]
    print("Warning: Due to the choose number -n, the choose list is reduced to", choose_list)
elif choose_number > len(choose_list):
    while choose_number > len(choose_list):
        i = pool.pop()
        if i not in choose_list: choose_list.append(i)

choose_list.sort()

if outputfile is None:
    outputfile = "Subsquare_" + "_".join(map(str, choose_list)) + ".kml"

# print('Choose number is ', choose_number)
# print('Choose list is ', choose_list)
# print('Output file is ', outputfile)


#######################
# Printing KML

#open file for export
with open(outputfile, "w") as file:

    file.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
    file.write("<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n")
    file.write("  <Document>\n")
    file.write(f"    <name>Subgraph {choose_list} of {g_size}x{g_size} Square</name>\n")
    for i in choose_list:
        x = i // 10
        y = i % 10 
        file.write(f"    <Placemark>\n")
        file.write(f"      <name>x{x}y{y}</name>\n")
        file.write(f"      <Point>\n")
        file.write(f"        <coordinates>\n")
        file.write(f"          {x},{y},0\n")
        file.write(f"        </coordinates>\n")
        file.write(f"      </Point>\n")
        file.write(f"    </Placemark>\n")

    file.write("  </Document>\n")
    file.write("</kml>\n")
