
 *** Effect of increasing number of memeory elements ***


This is a directory where our third experiment is performed.


results/
   - a directory
   - it contains our automatically generated results:
       Experiment3d_report.pdf
          - a detailed summary of the obtained results
          - for each number of memory elements, we analyze
            the obtained strategy values, number of iterations,
            and the execution times (in seconds, get by /usr/bin/time)
       Table3.txt
          - source data for Table 3

Template_building_r10_c4.blin
   - Plan of a one-floor building with 4 nodes representing a corridor
     and 10 nodes representing rooms (possible targets with blindness 0.0,
     target value 100, and attack time 112)
   - the structure is:
                    R       R       R       R
                    |       |       |       |
                    5       5       5       5
                    |       |       |       |
            R - 5 - C - 2 - C - 2 - C - 2 - C - 5 - R
                    |       |       |       |
                    5       5       5       5
                    |       |       |       |
                    R       R       R       R
   - each edge with length 2 stands for walking,
     the edges with length 5 represent unlocking doors
     and entering the particular rooms.


Prepare_experiment_3.sh
   - a bash script
   - generates input graphs with 1..8 memory elements to subdirectories
     Building_m_00[1-8]/

Run_experiment_3.sh
   - a bash script
   - runs the strategy computation (takes a bit longer time)

Summarize_experiment_3.sh
   - a bash script
   - extracts data from files Building_m_00[1-8]/*.out
     Building_m_00[1-8]/*.time, and creates the PDF file
     Experiment3d_report.pdf and Table3.txt stored in ./results/


All the bash scripts uses Makefile and other programs from ../tools/.
